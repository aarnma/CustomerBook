package com.programmer1.customerbook.OutBook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.R;
import com.programmer1.customerbook.helper.SQLiteHandler;
import com.programmer1.customerbook.helper.SessionManager;
import com.isapanah.awesomespinner.AwesomeSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by aaron on 2/27/2018.
 */

public class AddOutMail extends AppCompatActivity {

    private Button add_out;
    private EditText inputPackageNum;
    private EditText inputTo;
    private AwesomeSpinner inputBy;
    private EditText inputSubject;
    private EditText inputPages;
    private AwesomeSpinner method;
    private AwesomeSpinner location;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_out);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        inputPackageNum = findViewById(R.id.out_package_num);
        inputTo = findViewById(R.id.out_to);
        inputBy = findViewById(R.id.out_by);
        inputSubject = findViewById(R.id.out_subject);
        inputPages = findViewById(R.id.out_pages);
        method = findViewById(R.id.out_method);
        add_out = findViewById(R.id.btnAddOut);
        location = findViewById(R.id.out_location);

        Toolbar toolbar = findViewById(R.id.add_out_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");


        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        ArrayAdapter<CharSequence> staffAdapter = ArrayAdapter.createFromResource(this, R.array.method, android.R.layout.simple_spinner_item);
        method.setAdapter(staffAdapter, 0);

        ArrayAdapter<CharSequence> branchAdapter = ArrayAdapter.createFromResource(this, R.array.location, android.R.layout.simple_spinner_item);
        location.setAdapter(branchAdapter, 0);

        ArrayAdapter<CharSequence> byAdapter = ArrayAdapter.createFromResource(this, R.array.staff_name, android.R.layout.simple_spinner_item);
        inputBy.setAdapter(byAdapter, 0);


        method.setOnSpinnerItemClickListener(new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int position, String itemAtPosition) {
                //TODO YOUR ACTIONS

            }
        });

        location.setOnSpinnerItemClickListener(new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int position, String itemAtPosition) {
                //TODO YOUR ACTIONS

            }
        });

        inputBy.setOnSpinnerItemClickListener(new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int position, String itemAtPosition) {
                //TODO YOUR ACTIONS

            }
        });


        add_out.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                String package_num = inputPackageNum.getText().toString();
                String to = inputTo.getText().toString();
                String by = inputBy.getSelectedItem();
                String subject = inputSubject.getText().toString();
                String pages = inputPages.getText().toString();
                String method_sel = method.getSelectedItem();
                String location_sel = location.getSelectedItem();

                if (!package_num.isEmpty() && !to.isEmpty() &&
                        inputBy.isSelected() && method.isSelected() && !subject.isEmpty() && !pages.isEmpty()
                        && location.isSelected()) {
                    add_in_to_database(package_num, to,
                            by, subject, pages, method_sel, location_sel);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please fill up all fields", Toast.LENGTH_LONG)
                            .show();
                }


            }
        });


    }

    private void add_in_to_database(final String package_num,
                                    final String to, final String by
            , final String subject, final String pages, final String method_sel, final String location_sel
    ) {

        pDialog.setMessage("Adding ...");
        showDialog();

        RequestBody form = new FormBody.Builder()
                .add("package_num", package_num)
                .add("to", to)
                .add("by", by)
                .add("subject", subject)
                .add("pages", pages)
                .add("method_sel", method_sel)
                .add("location_sel", location_sel)
                .build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        // GET request
        Request request = new Request.Builder()
                .url(Configure.URL_ADD_OUT)
                .post(form)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideDialog();
                        Toast.makeText(getBaseContext(),
                                "Server is disconnected", Toast.LENGTH_LONG).show();

                    }
                });
                System.out.println(e);

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                hideDialog();
                final String res = response.body().string();

                String status = null;
                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(res);
                    status = Jobject.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String finalStatus = status != null ? status : "500";


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (finalStatus) {
                            case "200":
                                Toast.makeText(getApplicationContext(),
                                        "New Out Mail added", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent();
                                intent.putExtra("data", res);
                                setResult(RESULT_OK, intent);
                                finish();
                                break;

                            case "500":
                                Toast.makeText(getApplicationContext(),
                                        "Server is busy", Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(getApplicationContext(),
                                        "Server is disconnected", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}