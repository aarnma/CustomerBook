package com.programmer1.customerbook.OutBook;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cocosw.bottomsheet.BottomSheet;
import com.programmer1.customerbook.R;

import java.util.ArrayList;

/**
 * Created by aaron on 2/27/2018.
 */

public class OutMailAdapter extends RecyclerView.Adapter<OutMailAdapter.OutailHolder> {
    private Activity context;
    private ArrayList<OutMail> outmails;
    private String right;


    public OutMailAdapter(Activity context, ArrayList<OutMail> outmails, String right) {
        this.context = context;
        this.outmails = outmails;
        this.right = right;

    }

    @Override
    public OutMailAdapter.OutailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OutMailAdapter.OutailHolder(this, context,
                LayoutInflater.from(parent.getContext()).inflate(R.layout.out_mail_layout, parent, false), right);
    }


    @Override
    public void onBindViewHolder(final OutMailAdapter.OutailHolder holder, int position) {
        OutMail mail = outmails.get(position);

        holder.getNumber().setText(mail.getNumber());
        holder.getDate().setText(mail.getDate());
        holder.getPackage_num().setText(mail.getPackage_num());
        holder.getTo().setText(mail.getTo());
        holder.getBy().setText(mail.getBy());
        holder.getSubject().setText(mail.getSubject());
        holder.getPages().setText(mail.getPages());
        holder.getMethod().setText(mail.getMethod());
        holder.getLocation().setText(mail.getLocation());


    }

    @Override
    public int getItemCount() {
        return outmails.size();
    }


    public class OutailHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


        protected TextView number;
        protected TextView date;
        protected TextView package_num;
        protected TextView to;
        protected TextView by;
        protected TextView method;
        protected TextView subject;
        protected TextView pages;
        protected TextView location;
        protected Activity mContext;

        public OutailHolder(OutMailAdapter outMailAdapter, Activity context, View itemView, String right) {
            super(itemView);
            number = itemView.findViewById(R.id.reference_out_num_content);
            date = itemView.findViewById(R.id.out_date_content);
            package_num = itemView.findViewById(R.id.out_package_num_content);
            to = itemView.findViewById(R.id.out_to_content);
            method = itemView.findViewById(R.id.out_method_content);
            subject = itemView.findViewById(R.id.out_subject_content);
            pages = itemView.findViewById(R.id.out_pages_content);
            by = itemView.findViewById(R.id.out_by_content);
            location = itemView.findViewById(R.id.out_location_content);
            mContext = context;
            itemView.setOnClickListener(this);
        }

        public TextView getNumber() {
            return number;
        }

        public TextView getDate() {
            return date;
        }

        public TextView getPackage_num() {
            return package_num;
        }

        public TextView getTo() {
            return to;
        }

        public TextView getBy() {
            return by;
        }

        public TextView getMethod() {
            return method;
        }

        public TextView getSubject() {
            return subject;
        }

        public TextView getPages() {
            return pages;
        }

        public TextView getLocation() {
            return location;
        }

        @Override
        public void onClick(View view) {
            final String ref_num = number.getText().toString();
            final String pages_str = pages.getText().toString();

            BottomSheet.Builder btsheet = new BottomSheet.Builder(mContext);
            btsheet.sheet(R.menu.bottom_menu_modify);
            btsheet.title("Operating " + ref_num)
                    .listener(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case R.id.modify:

                                    Intent intent = new Intent(context, ModifyOutMail.class);
                                    intent.putExtra("ref_num", ref_num);
                                    intent.putExtra("pages", pages_str);
                                    ((Activity) mContext).startActivityForResult(intent,10);

                                    break;
                            }
                        }
                    }).show();

        }

    }
}
