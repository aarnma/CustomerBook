package com.programmer1.customerbook.OutBook;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;

import com.programmer1.customerbook.R;
import com.programmer1.customerbook.helper.SQLiteHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by aaron on 5/29/2018.
 */

public class DisplayOutMailResult extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_results);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Toolbar toolbar = findViewById(R.id.search_result_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        final SQLiteHandler db = new SQLiteHandler(getApplicationContext());
        HashMap<String, String> user = db.getUserDetails();
        String right = user.get("right");

        try {
            OutMailFragment fragment = new OutMailFragment(stringToJson(getIntent().getStringExtra("data")), right, DisplayOutMailResult.this);
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.result_container, fragment)
                    .commit();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public ArrayList<OutMail> stringToJson(String str) throws JSONException {

        ArrayList<OutMail> result = new ArrayList<>();
        JSONObject Jobject = new JSONObject(str);
        JSONArray Jarray = Jobject.getJSONArray("output");

        for (int i = 0; i < Jarray.length(); i++) {
            JSONObject object  = Jarray.getJSONObject(i);

            OutMail out = new OutMail(object.getString("ref_num"),
                    object.getString("date"),  object.getString("to"),
                    object.getString("package_num"), object.getString("method"),
                    object.getString("by"), object.getString("pages"),
                    object.getString("subject"), object.getString("location"));

            result.add(out);
        }
        return result;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
