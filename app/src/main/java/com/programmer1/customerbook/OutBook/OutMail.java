package com.programmer1.customerbook.OutBook;

/**
 * Created by aaron on 2/27/2018.
 */

public class OutMail {

    private String number;
    private String date;
    private String to;
    private String package_num;
    private String method;                                                                                                                                                                                                                              ;
    private String by;
    private String pages;
    private String subject;
    private String location;

    public OutMail(String number, String date, String to, String package_num, String method, String by, String pages, String subject, String location) {
        this.number = number;
        this.date = date;
        this.to = to;
        this.package_num = package_num;
        this.method = method;
        this.by = by;
        this.pages = pages;
        this.subject = subject;
        this.location = location;
    }

    public String getNumber() {
        return number;
    }

    public String getDate() {
        return date;
    }

    public String getTo() {
        return to;
    }

    public String getPackage_num() {
        return package_num;
    }

    public String getMethod() {
        return method;
    }

    public String getBy() {
        return by;
    }

    public String getPages() {
        return pages;
    }

    public String getSubject() {
        return subject;
    }

    public String getLocation() {
        return location;
    }
}
