package com.programmer1.customerbook.ContractBook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.R;
import com.isapanah.awesomespinner.AwesomeSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Programmer 1 on 2018-01-19.
 */

public class AddContract extends AppCompatActivity {

    private Button add_contract;
    private EditText inputFirstName;
    private EditText inputLastName;
    private EditText inputPhone;
    private EditText inputContractAmount;
    private EditText inputMemo;
    private EditText inputEmail;
    private AwesomeSpinner staff;
    private AwesomeSpinner branch;
    private ProgressDialog pDialog;
    private String branch_id = "";

    private CheckBox work_permit;
    private CheckBox work_permit_extension;

    private CheckBox study_permit;
    private CheckBox study_permit_extension;


    private CheckBox vistor_spuer_visa;
    private CheckBox vistor_family_visit;
    private CheckBox vistor_family_visit_parent;
    private CheckBox vistor_tourism;
    private CheckBox vistor_canada_usa;
    private CheckBox vistor_extension;

    private CheckBox spouse_sponsorship;
    private CheckBox parent_sponsorship;
    private CheckBox child_sponsorship;
    private CheckBox adopted_child_sponsorship;

    private CheckBox skilled_worker_ee;
    private CheckBox skilled_worker_pnp;

    private CheckBox entrepreneur_bc_pnp;

    private CheckBox citizenship_adult;
    private CheckBox citizenship_parent;
    private CheckBox citizenship_minor;
//    private CheckBox citizenship_adoption;

    private CheckBox appeal_family;
    private CheckBox appeal_individual;
    private CheckBox appeal_pr;

    private CheckBox other_pr_renew;
    private CheckBox other_voluntary;

    private CheckBox other_lmia;
    private CheckBox other_refugee;
    private CheckBox other_us_visa;
    private CheckBox other_atip;
    private CheckBox other_other;
    private EditText inputOther;

    private EditText inputGst;

    private ArrayList<CheckBox> checkBoxes;

    //This is the start to claim rehab checkbox, step 2
    private CheckBox other_rehab;
    // This is the end to claim rehab checkbox

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contract);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        initItems();


        Toolbar toolbar = findViewById(R.id.add_contract_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");


        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);


        ArrayAdapter<CharSequence> staffAdapter = ArrayAdapter.createFromResource(this, R.array.staff_name, android.R.layout.simple_spinner_item);
        staff.setAdapter(staffAdapter, 0);

        ArrayAdapter<CharSequence> branchAdapter = ArrayAdapter.createFromResource(this, R.array.branch_name, android.R.layout.simple_spinner_item);
        branch.setAdapter(branchAdapter, 0);


        staff.setOnSpinnerItemClickListener(new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int position, String itemAtPosition) {
                //TODO YOUR ACTIONS

            }
        });


        branch.setOnSpinnerItemClickListener(new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int position, String itemAtPosition) {
                if (itemAtPosition.equals("RichMond HQ")) {
                    branch_id = "01";
                } else if (itemAtPosition.equals("Burnaby Crystal Mall")) {
                    branch_id = "02";
                }
            }
        });

        add_contract.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String firstname = inputFirstName.getText().toString().trim();
                String lastname = inputLastName.getText().toString().trim();
                String phone = inputPhone.getText().toString().trim();
                String contract_amount = inputContractAmount.getText().toString().trim();
                String staff_name = staff.getSelectedItem().trim();
                String branch_name = branch.getSelectedItem().trim();
                String memo = inputMemo.getText().toString().trim();
                String email = inputEmail.getText().toString().trim();
                String gst = inputGst.getText().toString().trim();

                ArrayList<String> services = new ArrayList<>();
                for (CheckBox box : checkBoxes) {
                    if (box.isChecked()) {
                        if(box.getText().equals("Other")){
                            services.add("others: " + inputOther.getText().toString());
                        }

                        else{services.add(box.getText().toString());}
                    }
                }

                String cases = TextUtils.join(", ", services.toArray());

                if (!firstname.isEmpty() && !lastname.isEmpty() && !phone.isEmpty() &&
                        !contract_amount.isEmpty() && staff.isSelected() && branch.isSelected()
                        && !branch_id.isEmpty() && !gst.isEmpty()) {
                    add_contract_to_database(firstname, lastname, phone,
                            contract_amount, staff_name, branch_name, branch_id, cases, memo, email, gst);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please fill up all fields", Toast.LENGTH_LONG)
                            .show();
                }


            }
        });


    }

    private void initItems() {
        inputFirstName = findViewById(R.id.first_name_contract);
        inputLastName = findViewById(R.id.last_name_contract);
        inputPhone = findViewById(R.id.phone_contract);
        inputContractAmount = findViewById(R.id.amount_contract);
        inputMemo = findViewById(R.id.contract_memo);
        staff = findViewById(R.id.select_staff);
        branch = findViewById(R.id.select_branch);
        add_contract = findViewById(R.id.btnAddContract);
        inputEmail = findViewById(R.id.email_contract);
        inputGst = findViewById(R.id.contract_gst);

        work_permit = findViewById(R.id.work_permit_first_checkbox);
        work_permit_extension = findViewById(R.id.work_permit_extension_checkbox);

        study_permit = findViewById(R.id.study_permit_first_checkbox);
        study_permit_extension = findViewById(R.id.study_permit_extension_checkbox);


        vistor_spuer_visa = findViewById(R.id.visitor_visa_super_checkbox);
        vistor_family_visit = findViewById(R.id.visitor_visa_family_checkbox);
        vistor_family_visit_parent = findViewById(R.id.visitor_visa_family_parent_checkbox);
        vistor_tourism = findViewById(R.id.visitor_visa_tourism_checkbox);
        vistor_canada_usa = findViewById(R.id.visitor_visa_canada_us_checkbox);
        vistor_extension = findViewById(R.id.visitor_visa_extension_checkbox);

        spouse_sponsorship = findViewById(R.id.spouse_checkbox);
        parent_sponsorship = findViewById(R.id.parents_grandparents_checkbox);
        child_sponsorship = findViewById(R.id.dependent_children_checkbox);
        adopted_child_sponsorship = findViewById(R.id.adopted_children_checkbox);

        skilled_worker_ee = findViewById(R.id.express_entry_checkbox);
        skilled_worker_pnp = findViewById(R.id.skilled_pnp_checkbox);

        entrepreneur_bc_pnp = findViewById(R.id.entrepreneur_pnp_checkbox);

        citizenship_adult = findViewById(R.id.citizenship_adult_checkbox);
        citizenship_parent = findViewById(R.id.citizenship_parent_checkbox);
        citizenship_minor = findViewById(R.id.citizenship_minor_checkbox);

        appeal_family = findViewById(R.id.family_appeal_checkbox);
        appeal_individual = findViewById(R.id.individual_appeal_checkbox);
        appeal_pr = findViewById(R.id.pr_appeal_checkbox);

        other_pr_renew = findViewById(R.id.pr_renew_checkbox);
        other_voluntary = findViewById(R.id.voluntary_renunciation_of_pr_checkbox);

        other_lmia = findViewById(R.id.lmia_checkbox);
        other_refugee = findViewById(R.id.refugee_checkbox);
        other_us_visa = findViewById(R.id.us_visa_checkbox);
        other_atip = findViewById(R.id.atip_checkbox);
        other_other = findViewById(R.id.others_checkbox);
        inputOther = findViewById(R.id.others_content);




        //This is the start to initialize rehab checkbox, step 3
        other_rehab = findViewById(R.id.rehab_checkbox);
        //This is the end to initialize rehab checkbox




        checkBoxes = new ArrayList<>();

        checkBoxes.add(work_permit);
        checkBoxes.add(work_permit_extension);

        checkBoxes.add(study_permit);
        checkBoxes.add(study_permit_extension);

        checkBoxes.add(vistor_spuer_visa);
        checkBoxes.add(vistor_family_visit);
        checkBoxes.add(vistor_family_visit_parent);
        checkBoxes.add(vistor_tourism);
        checkBoxes.add(vistor_canada_usa);
        checkBoxes.add(vistor_extension);

        checkBoxes.add(spouse_sponsorship);
        checkBoxes.add(parent_sponsorship);
        checkBoxes.add(child_sponsorship);
        checkBoxes.add(adopted_child_sponsorship);

        checkBoxes.add(skilled_worker_ee);
        checkBoxes.add(skilled_worker_pnp);

        checkBoxes.add(entrepreneur_bc_pnp);

        checkBoxes.add(citizenship_adult);
        checkBoxes.add(citizenship_minor);
        checkBoxes.add(citizenship_parent);

        checkBoxes.add(appeal_family);
        checkBoxes.add(appeal_individual);
        checkBoxes.add(appeal_pr);

        checkBoxes.add(other_pr_renew);
        checkBoxes.add(other_voluntary);

        checkBoxes.add(other_lmia);
        checkBoxes.add(other_refugee);
        checkBoxes.add(other_us_visa);
        checkBoxes.add(other_atip);
        checkBoxes.add(other_other);


        //This is the start to add rehab checkbox to box list, Step 4
        checkBoxes.add(other_rehab);
        //This is the end to add rehab checkbox to box list

    }

    private void add_contract_to_database(final String firstname, final String lastname,
                                          final String phone, final String contract_amount
            , final String staff_name, final String branch_name, final String branch_id, final String class_name,
                                          final String memo, final String email, final String gst) {

        pDialog.setMessage("Adding ...");
        showDialog();

        RequestBody form = new FormBody.Builder()
                .add("firstname", firstname)
                .add("lastname", lastname)
                .add("phone", phone)
                .add("contract_amount", contract_amount)
                .add("staff_name", staff_name)
                .add("branch_name", branch_name)
                .add("branch_id", branch_id)
                .add("class", class_name)
                .add("memo", memo)
                .add("email", email)
                .add("gst", gst)
                .build();

//         client = new OkHttpClient();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
        // GET request
        Request request = new Request.Builder()
                .url(Configure.URL_ADD_CONTRACT)
                .post(form)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideDialog();
                        Toast.makeText(getApplicationContext(),
                                "Server is disconnected", Toast.LENGTH_LONG).show();

                    }
                });

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String res = response.body().string();

                String status = null;
                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(res);
                    status = Jobject.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String finalStatus = status != null ? status : "500";

                hideDialog();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (finalStatus) {
                            case "200":
                                Toast.makeText(getApplicationContext(),
                                        "New contract added", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent();
                                intent.putExtra("data", res);
                                setResult(RESULT_OK, intent);
                                finish();
                                break;

                            case "400":
                                Toast.makeText(getApplicationContext(),
                                        "Pdf failed to sent, make sure email address is correct", Toast.LENGTH_LONG).show();
                                break;

                            case "500":
                                Toast.makeText(getApplicationContext(),
                                        "Server is busy", Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(getApplicationContext(),
                                        "Server is disconnected", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
