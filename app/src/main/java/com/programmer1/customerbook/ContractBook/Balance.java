package com.programmer1.customerbook.ContractBook;

import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Programmer 1 on 2018-02-14.
 */

public class Balance extends AppCompatActivity{

    private String url;

    public Balance(String url) {
        this.url = url;
    }


    public void setBalanceHelper(String contract_num, String amount, final TextView balance_msg){

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        RequestBody form = new FormBody.Builder()
                .add("contract_num", contract_num)
                .add("amount", amount)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     balance_msg.setText("Server error, try again");

                                 }
                             });
                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             String status = null;
                             JSONObject Jobject = null;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");

                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status != null ? status : "500";
                             final JSONObject finalJobject = Jobject;

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     switch (finalStatus) {
                                         case "200":

                                             try {

                                                 String balance = finalJobject.getString("balance");
                                                 balance_msg.setText(balance);

                                             } catch (JSONException e) {
                                                 e.printStackTrace();
                                             }

                                             break;

                                         case "404":

                                             balance_msg.setText("No found");

                                             break;
                                         default:

                                             balance_msg.setText("Server error, try again");
                                             break;
                                     }
                                 }
                             });
                         }
                     }
        );
    }
}
