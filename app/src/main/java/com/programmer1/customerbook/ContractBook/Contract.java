package com.programmer1.customerbook.ContractBook;

/**
 * Created by aaron on 1/29/2018.
 */

public class Contract {

    private String date;
    private String branch;
    private String first_name;
    private String last_name;
    private String phone;
    private String contract_amount;
    private String staff;
    private String contract_num;
    private String file_id;
    private String receipt_num;
    private String status;
    private String balance;
    private String class_name;
    private String memo;


    public Contract(String date, String branch, String first_name, String last_name, String phone, String contract_amount,
                   String staff, String contract_num, String file_id, String class_name, String receipt_num, String balance,
                    String status, String memo) {
        this.date = date;
        this.branch = branch;
        this.first_name = first_name;
        this.last_name = last_name;
        this.phone = phone;
        this.contract_amount = contract_amount;
        this.staff = staff;
        this.contract_num = contract_num;
        this.file_id = file_id;
        this.receipt_num = receipt_num;
        this.balance = balance;
        this.class_name = class_name;
        this.status = status;
        this.memo = memo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getFirst_name() {
        return first_name;
    }
    public String getLast_name() {
        return last_name;
    }
    public String getPhone() {
        return phone;
    }
    public String getContract_amount() {
        return contract_amount;
    }
    public String getContract_num() {
        return contract_num;
    }
    public String getFile_id() {
        return file_id;
    }
    public String getStaff() {
        return staff;
    }
    public String getReceipt() {
        return receipt_num;
    }
    public String getBalance() {
        return balance;
    }
    public String getClass_name() {
        return class_name;
    }
    public String getStatus() {
        return status;
    }
    public String getMemo() {
        return memo;
    }

}