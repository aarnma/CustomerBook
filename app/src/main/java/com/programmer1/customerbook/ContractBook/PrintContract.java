package com.programmer1.customerbook.ContractBook;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.programmer1.customerbook.BuildConfig;
import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.read.biff.BiffException;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;
import me.zhouzhuo.zzexcelcreator.ZzExcelCreator;
import me.zhouzhuo.zzexcelcreator.ZzFormatCreator;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.programmer1.customerbook.Configure.hideKeyboard;

/**
 * Created by aaron on 5/22/2018.
 */

public class PrintContract extends AppCompatActivity {

    private String PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Contracts/";
    private String filename;
    private EditText start_date;
    private EditText end_date;
    private ProgressDialog pDialog;
    private CheckBox excel;
    private JSONObject summary;
    Date start, end;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_contract);

        start_date = findViewById(R.id.print_contract_from);
        end_date = findViewById(R.id.print_contract_to);
        Button print_btn = findViewById(R.id.btnPrintContract);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        excel = findViewById(R.id.print_contract_excel);

        Toolbar toolbar = findViewById(R.id.print_contract_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        }


        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener from_date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

//                start = year + monthOfYear +
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.CANADA);
                String date_str = sdf.format(myCalendar.getTime());
                try {
                    start = sdf.parse(date_str);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                start_date.setText(date_str);
            }

        };

        start_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                hideKeyboard(PrintContract.this);
                new DatePickerDialog(PrintContract.this, from_date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        final DatePickerDialog.OnDateSetListener to_date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.CANADA);
                String date_str = sdf.format(myCalendar.getTime());
                try {
                    end = sdf.parse(date_str);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                end_date.setText(date_str);
            }

        };

        end_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                hideKeyboard(PrintContract.this);
                new DatePickerDialog(PrintContract.this, to_date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        print_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (start == null || end == null) {
                    Toast.makeText(getApplicationContext(),
                            "Please fill up start and end date!", Toast.LENGTH_LONG).show();
                } else if (!start.after(end)) {
                    calculateContract(start_date.getText().toString(), end_date.getText().toString());
//                    if(excel.isChecked()){
//                        getContractList(start_date.getText().toString(), end_date.getText().toString());
//                    }
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Start date must before end date", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    private void calculateContract(final String from, final String to){

        pDialog.setMessage("Getting data from database");
        showDialog();

        RequestBody form = new FormBody.Builder()
                .add("from", from)
                .add("to", to)
                .build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(600, TimeUnit.SECONDS)
                .writeTimeout(600, TimeUnit.SECONDS)
                .readTimeout(600, TimeUnit.SECONDS)
                .build();

        Request request = new Request.Builder()
                .url(Configure.URL_CALCULATE_CONTRACT)
                .post(form)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
//                hideDialog();
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        hideDialog();
                        Toast.makeText(getApplicationContext(),
                                "Search server disconnected/failed/timeout", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String res = response.body().string();

                String status = null;
                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(res);
                    status = Jobject.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String finalStatus = status != null ? status : "500";


                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        switch (finalStatus) {
                            case "200":
                                try {
                                    JSONObject Jobject = new JSONObject(res);
                                    JSONArray Jarray = Jobject.getJSONArray("output");
                                    summary  = Jarray.getJSONObject(0);
                                    getContractList(from, to);

//                                    hideDialog();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                break;

                            case "404":
                                hideDialog();
                                Toast.makeText(getApplicationContext(),
                                        "No Contract found", Toast.LENGTH_LONG).show();
                                break;

                            case "500":
                                hideDialog();
                                Toast.makeText(getApplicationContext(),
                                        "Server is busy", Toast.LENGTH_LONG).show();
                                break;
                            default:
                                hideDialog();
                                Toast.makeText(getApplicationContext(),
                                        "Server is disconnected", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

        });
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void getContractList(final String from, final String to){

        pDialog.setMessage("Getting data from database");
        showDialog();

        RequestBody form = new FormBody.Builder()
                .add("from", from)
                .add("to", to)
                .build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        Request request = new Request.Builder()
                .url(Configure.URL_PRINT_CONTRACT)
                .post(form)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        hideDialog();
                        Toast.makeText(getApplicationContext(),
                                "Search server disconnected", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String res = response.body().string();

                String status = null;
                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(res);
                    status = Jobject.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String finalStatus = status != null ? status : "500";


                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        switch (finalStatus) {
                            case "200":
                                try {
                                    Intent intent = new Intent(PrintContract.this, DisplayContractResult.class);
                                    intent.putExtra("count", summary.getString("count"));
                                    intent.putExtra("total_amount", summary.getString("total_amount"));
                                    intent.putExtra("from", summary.getString("from"));
                                    intent.putExtra("to", summary.getString("to"));
                                    intent.putExtra("expense", summary.getString("expense"));
                                    intent.putExtra("data", res);
                                    startActivity(intent);

                                    if(excel.isChecked()){
                                        pDialog.setMessage("Generating excel...");
                                        print(from, to, res);
                                    }
                                    hideDialog();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                break;

                            case "404":
                                hideDialog();
                                Toast.makeText(getApplicationContext(),
                                        "No Contract found", Toast.LENGTH_LONG).show();
                                break;

                            case "500":
                                hideDialog();
                                Toast.makeText(getApplicationContext(),
                                        "Server is busy", Toast.LENGTH_LONG).show();
                                break;
                            default:
                                hideDialog();
                                Toast.makeText(getApplicationContext(),
                                        "Server is disconnected", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

        });


    }
    public void print(String start, String end, String data) throws JSONException {

        createExcel(start, end);

        JSONObject Jobject = new JSONObject(data);
        JSONArray Jarray = Jobject.getJSONArray("output");
        String[][] array = new String[Jarray.length()+1][14];

        array[0][0] = "Contract #";
        array[0][1] = "Date";
        array[0][2] = "File #";
        array[0][3] = "Contract Amount";
        array[0][4] = "Firstname";
        array[0][5] = "Lastname";
        array[0][6] = "Phone";
        array[0][7] = "Branch";
        array[0][8] = "Class";
        array[0][9] = "Balance";
        array[0][10] = "Receipt #";
        array[0][11] = "Staff";
        array[0][12] = "Memo";
        array[0][13] = "Status";

        for (int i=1; i <= Jarray.length(); i++) {
            JSONObject object = Jarray.getJSONObject(i-1);
            array[i][0] = object.getString("contract_num");
            array[i][1] = object.getString("date");
            array[i][2] = object.getString("file_num");
            array[i][3] = object.getString("contract_amount");
            array[i][4] = object.getString("firstname");
            array[i][5] = object.getString("lastname");
            array[i][6] = object.getString("phone");
            array[i][7] = object.getString("branch");
            array[i][8] = object.getString("class");
            array[i][9] = object.getString("balance");
            array[i][10] = object.getString("receipt_num");
            array[i][11] = object.getString("staff");
            array[i][12] = object.getString("memo");
            array[i][13] = object.getString("status");
        }

        for (int i=1; i <= Jarray.length(); i++) {
            System.out.println(array[i][11]);

        }

        insertToExcel(array);

    }

    @SuppressLint("StaticFieldLeak")
    private void createExcel(String start, String end) {
        filename = "Contract_" + start + "_to_" + end;
        String sheetName = "Sheet1";

        new AsyncTask<String, Void, Integer>() {

            @Override
            protected Integer doInBackground(String... params) {
                try {
                    ZzExcelCreator
                            .getInstance()
                            .createExcel(PATH, params[0])
                            .createSheet(params[1])
                            .close();
                    return 1;
                } catch (IOException | WriteException e) {
                    e.printStackTrace();
                    return 0;
                }
            }

            @Override
            protected void onPostExecute(Integer aVoid) {
                super.onPostExecute(aVoid);
                if (aVoid == 1) {
                } else {
                    Toast.makeText(PrintContract.this, "表格创建失败！", Toast.LENGTH_SHORT).show();
                    hideDialog();
                }
            }
        }.execute(filename, sheetName);
    }

    @SuppressLint("StaticFieldLeak")
    private void insertToExcel(final String[][] array){

        new AsyncTask<String, Void, Integer>() {

            @Override
            protected Integer doInBackground(String... params) {
                try {
                    WritableCellFormat format = ZzFormatCreator
                            .getInstance()
                            .createCellFont(WritableFont.ARIAL)
                            .setAlignment(Alignment.CENTRE, VerticalAlignment.CENTRE)
                            .setFontSize(14)
                            .setFontColor(Colour.DARK_GREEN)
                            .getCellFormat();


                    ZzExcelCreator
                            .getInstance()
                            .openExcel(new File(PATH + filename + ".xls"))
                            .openSheet(0);

                    for(int row = 0; row < array.length; row++){
                        for(int col = 0; col < array[0].length; col++){
                            ZzExcelCreator.getInstance().setColumnWidth(col, 25)
                                    .setRowHeight(row, 400)
                                    .fillContent(col, row, array[row][col], format);

                        }
                    }

                    ZzExcelCreator.getInstance().close();

                    return 1;
                } catch (IOException | WriteException | BiffException e) {
                    e.printStackTrace();
                    return 0;
                }
            }

            @Override
            protected void onPostExecute(Integer aVoid) {
                super.onPostExecute(aVoid);
                if (aVoid == 1) {
                    Toast.makeText(PrintContract.this, "Excel生成成功！", Toast.LENGTH_SHORT).show();
                    openExcel();
                } else {
                    Toast.makeText(PrintContract.this, "Excel生成失败！", Toast.LENGTH_SHORT).show();
                }
                hideDialog();
            }
        }.execute();
    }

    private void openExcel(){
        File file = new File(PATH + filename + ".xls");
        Intent intent = new Intent(Intent.ACTION_VIEW);

        if(Build.VERSION.SDK_INT>=24){
            try{
                intent.setDataAndType( FileProvider.getUriForFile(PrintContract.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        file),"application/vnd.ms-excel");
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        else{
            intent.setDataAndType(Uri.fromFile(file),"application/vnd.ms-excel");
        }



        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(PrintContract.this,"No Application available to viewExcel", Toast.LENGTH_SHORT).show();
        }
    }
}