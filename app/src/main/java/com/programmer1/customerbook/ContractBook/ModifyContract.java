package com.programmer1.customerbook.ContractBook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.Login.LoginActivity;
import com.programmer1.customerbook.R;
import com.programmer1.customerbook.helper.SQLiteHandler;
import com.programmer1.customerbook.helper.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by aaron on 1/31/2018.
 */

public class ModifyContract  extends AppCompatActivity {

    private TextView name;
    private Button btnSave;
    private EditText inputPhone;
    private EditText inputAmount;
    private SessionManager session;
    private SQLiteHandler db;
    private ProgressDialog pDialog;
    private HashMap<EditText, Boolean> editTextHashMap;

    private EditText inputMemo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_contract);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        // Session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        name = findViewById(R.id.contract_customer_name);
        inputPhone = findViewById(R.id.modify_phone);
        inputAmount= findViewById(R.id.contract_amount);
        btnSave = findViewById(R.id.saveContract);

        inputMemo =  findViewById(R.id.modify_contract_memo);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        Toolbar toolbar = findViewById(R.id.modify_contract_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");


        name.setText(getIntent().getStringExtra("contract_name"));
        inputPhone.setText(getIntent().getStringExtra("contract_phone"));
        inputAmount.setText(getIntent().getStringExtra("contract_amount"));

        inputMemo.setText(getIntent().getStringExtra("contract_memo"));


        editTextHashMap = new HashMap<>();
        editTextHashMap.put(inputPhone, false);
        editTextHashMap.put(inputAmount, false);
        editTextHashMap.put(inputMemo, false);



        addTextChangeListener(inputPhone, editTextHashMap);
        addTextChangeListener(inputAmount, editTextHashMap);
        addTextChangeListener(inputMemo, editTextHashMap);


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HashMap<String, String> dataHashMap = new HashMap<>();
                String contract_num = getIntent().getStringExtra("contract_num");

                // Get a set of the entries
                Set set = editTextHashMap.entrySet();

                // Get an iterator
                Iterator i = set.iterator();

                // Display elements
                while(i.hasNext()) {
                    Map.Entry me = (Map.Entry)i.next();
                    EditText text = (EditText) me.getKey();

                    switch (text.getId()) {

                        case R.id.modify_phone:
                            System.out.print( "Phone: ");
                            Boolean check = (Boolean) me.getValue();
                            if(check){
                                dataHashMap.put("phone", inputPhone.getText().toString());
                            }
                            break;

                        case R.id.contract_amount:
                            System.out.print( "contract_amount: ");
                            check = (Boolean) me.getValue();
                            if(check){
                                dataHashMap.put("contract_amount", inputAmount.getText().toString());
                            }
                            break;

                        case R.id.modify_contract_memo:
                            System.out.print( "Memo: ");
                            check = (Boolean) me.getValue();
                            if(check){
                                dataHashMap.put("memo", inputMemo.getText().toString().replace("\n", "/m").trim());
                            }
                            break;
                        default:
                            break;

                    }

                }


                if(dataHashMap.isEmpty()){
                    Toast.makeText(getApplicationContext(),
                            "Nothing changed", Toast.LENGTH_LONG).show();
                }
                else{
                    update(contract_num, dataHashMap);
                }
            }
        });
    }


    private void update(final String contract_num, HashMap<String, String> map) {

        pDialog.setMessage("Modifying ...");
        showDialog();


        Set set = map.entrySet();
        Iterator i = set.iterator();

        //chnage to int
        String json_str ="{\"contract_num\": \"" + contract_num +"\", " + "\"data\": [";
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
//            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue());
            json_str += "{\"" + me.getKey().toString() + "\": \"" + me.getValue().toString() + "\"}," ;
        }

        json_str=json_str.substring(0, json_str.length()-1)+"]}";

        final MediaType mediaType = MediaType.parse("application/json");

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        // GET request
        Request request = new Request.Builder()
                .url(Configure.URL_UPDATE_CONTRACT)
                .post(RequestBody.create(mediaType, json_str))
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        hideDialog();
                        Toast.makeText(getApplicationContext(),
                                "Cannot connect to server", Toast.LENGTH_LONG).show();

                    }
                });

                System.out.println(e);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                hideDialog();
                final String res = response.body().string();
                System.out.println("response");
                System.out.println(res);

                String status = null;
                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(res);
                    status = Jobject.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String finalStatus = status != null ? status : "500";
                final JSONObject finalJobject = Jobject;


                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        switch (finalStatus) {
                            case "200":
                                Toast.makeText(getApplicationContext(),
                                        "Modify successfully", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent();
                                intent.putExtra("key", name.getText().toString());
                                setResult(RESULT_OK, intent);
                                finish();
                                break;

                            case "400":
                                Toast.makeText(getApplicationContext(),
                                        "Wrong data", Toast.LENGTH_LONG).show();
                                break;
                            case "404":
                                Toast.makeText(getApplicationContext(),
                                        "Contract no found", Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(getApplicationContext(),
                                        "Server is busy, try again", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

        });

    }

    public void addTextChangeListener(final EditText input, final HashMap<EditText, Boolean> map){
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                map.put(input, true);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }
}