package com.programmer1.customerbook.ContractBook;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.TextView;

import com.programmer1.customerbook.R;
import com.programmer1.customerbook.helper.SQLiteHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by aaron on 5/22/2018.
 */

public class DisplayContractResult extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_contract);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Toolbar toolbar = findViewById(R.id.print_contract_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        TextView from = findViewById(R.id.from_text);
        TextView to = findViewById(R.id.to_text);
        TextView amount = findViewById(R.id.amount_text);
        TextView total = findViewById(R.id.total_amount_text);
        TextView expense = findViewById(R.id.expense_con_text);

        from.setText(getIntent().getStringExtra("from"));
        to.setText(getIntent().getStringExtra("to"));
        amount.setText(getIntent().getStringExtra("count"));
        total.setText(getIntent().getStringExtra("total_amount"));
        expense.setText(getIntent().getStringExtra("expense"));

        final SQLiteHandler db = new SQLiteHandler(getApplicationContext());
        HashMap<String, String> user = db.getUserDetails();
        String right = user.get("right");

        try {
            ContractFragment fragment = new ContractFragment(stringToJson(getIntent().getStringExtra("data")), right, DisplayContractResult.this);
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.result_container, fragment)
                    .commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public ArrayList<Contract> stringToJson(String str) throws JSONException {

        ArrayList<Contract> result = new ArrayList<>();
        JSONObject Jobject = new JSONObject(str);
        JSONArray Jarray = Jobject.getJSONArray("output");

        for (int i = 0; i < Jarray.length(); i++) {
            JSONObject object  = Jarray.getJSONObject(i);
            Contract contract = new Contract(object.getString("date"),
                    object.getString("branch"),  object.getString("firstname"),
                    object.getString("lastname"), object.getString("phone"),
                    object.getString("contract_amount"), object.getString("staff"),
                    object.getString("contract_num"), object.getString("file_num"),  object.getString("class"),
                    object.getString("receipt_num"), object.getString("balance"), object.getString("status"),
                    object.getString("memo"));

            result.add(contract);
        }
        return result;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
