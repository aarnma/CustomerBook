package com.programmer1.customerbook.ContractBook;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cocosw.bottomsheet.BottomSheet;
import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.Connection.DataBaseConnection;
import com.programmer1.customerbook.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aaron on 1/29/2018.
 */

public class ContractAdapter extends RecyclerView.Adapter<ContractAdapter.ContractHolder> {
    private Activity context;
    private List<Contract> contracts;
    private String right;

    private  BottomSheet.Builder btsheet;


    public ContractAdapter(Activity context, ArrayList<Contract> contracts, String right) {
        this.context = context;
        this.contracts = contracts;
        this.right = right;

    }

    @Override
    public ContractAdapter.ContractHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContractAdapter.ContractHolder(this, context,
                LayoutInflater.from(parent.getContext()).inflate(R.layout.contract_layout, parent, false), right);
    }

    @Override
    public void onBindViewHolder(final ContractAdapter.ContractHolder holder, int position) {
        Contract contract = contracts.get(position);

        holder.getPhoneView().setText(contract.getPhone());
        holder.getBranchView().setText(contract.getBranch());
        holder.getFirst_nameView().setText(contract.getFirst_name());
        holder.getLast_nameView().setText(contract.getLast_name());
        holder.getContract_amountView().setText(contract.getContract_amount());
        holder.getReceipt_numView().setText(contract.getReceipt());

        holder.getContract_numView().setText(contract.getContract_num());
        holder.getFile_idView().setText(contract.getFile_id());
        holder.getStaffView().setText(contract.getStaff());
        holder.getDateView().setText(contract.getDate());
        holder.getBalanceView().setText(contract.getBalance());
        holder.getClassNameView().setText(contract.getClass_name());

//        String memo = contract.getMemo().replace("/m", "\n").trim();
        holder.getMemoView().setText(contract.getMemo());
        holder.getStatusView().setText(contract.getStatus());


    }

    @Override
    public int getItemCount() {
        return contracts.size();
    }

    public void removeAt(int position) {
        contracts.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, contracts.size());
    }


    public class ContractHolder extends RecyclerView.ViewHolder {
        protected TextView date;
        protected TextView branch;
        protected TextView first_name;
        protected TextView last_name;
        protected TextView phone;
        protected TextView contract_amount;
        protected TextView staff;
        protected TextView contract_num;
        protected TextView receipt_num;
        protected TextView file_id;
        protected TextView status;
        protected TextView balance;
        protected TextView class_name;
        protected ImageView arrow;
        protected Activity mContext;
        private String right_str;
        protected TextView memo;

        public ContractHolder(final ContractAdapter adapter, final Activity context, View itemView, final String right) {
            super(itemView);

            this.right_str = right;
            date = itemView.findViewById(R.id.contract_date_content);
            branch = itemView.findViewById(R.id.contract_branch_content);
            first_name = itemView.findViewById(R.id.contract_first_name_content);
            last_name = itemView.findViewById(R.id.contract_last_name_content);
            phone = itemView.findViewById(R.id.contract_phone_content);
            contract_amount = itemView.findViewById(R.id.contract_amount_content);
            staff = itemView.findViewById(R.id.contract_staff_content);
            contract_num = itemView.findViewById(R.id.contract_id_content);
            file_id = itemView.findViewById(R.id.file_id_content);
            receipt_num  = itemView.findViewById(R.id.contract_receipt_content);
            balance = itemView.findViewById(R.id.contract_balance_content);
            status = itemView.findViewById(R.id.contract_status_content);
            class_name  = itemView.findViewById(R.id.contract_class_content);
            memo  = itemView.findViewById(R.id.contract_memo_content);
            arrow = itemView.findViewById(R.id.contract_arrow_icon);
            mContext = context;


            arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String contract_num_str = contract_num.getText().toString();
                    final String status_str = status.getText().toString();
                    final String name_str = last_name.getText() + " " + first_name.getText();
                    final String phone_str = phone.getText().toString();
                    final String amount_str = contract_amount.getText().toString();
                    final String memo_str = memo.getText().toString();

                    btsheet = new BottomSheet.Builder(mContext);
                    String account_right = (right_str != null) ? right_str : "5";

                    if(status_str.equals("a")){
                        switch (account_right){
                            case "9":
                                btsheet.sheet(R.menu.bottom_menu);
                                break;
                            case  "7":
                                btsheet.sheet(R.menu.bottom_menu_for_7);
                                break;
                            case "5":
                                btsheet.sheet(R.menu.bottom_menu_for_5);
                                break;
                        }

                        btsheet.title("Operating " + contract_num_str)
                                .listener(new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case R.id.modify:

                                                System.out.println("Modify");
                                                Intent intent = new Intent(context, ModifyContract.class);
                                                intent.putExtra("contract_name", name_str);
                                                intent.putExtra("contract_phone", phone_str);
                                                intent.putExtra("contract_num", contract_num_str);
                                                intent.putExtra("contract_amount", amount_str);
                                                intent.putExtra("contract_memo", memo_str);

                                                ((Activity) mContext).startActivityForResult(intent,1);

                                                break;
                                            case R.id.delete:
                                                System.out.println("delete");
                                                DataBaseConnection connection =
                                                        new DataBaseConnection(Configure.URL_DELETE_CONTRACT,
                                                                Integer.parseInt(contract_num_str));
                                                connection.delete_contract(adapter, ContractHolder.this);
                                                break;
                                            case R.id.print:
                                                System.out.println("print");
                                                break;
                                        }
                                    }
                                }).show();
                    }
                    else if(status_str.equals("d")){
                        switch (account_right){
                            case "9":
                                btsheet.sheet(R.menu.deleted_bottom_menu);
                                break;
                            case  "7":
                                btsheet.sheet(R.menu.delete_bottom_menu_for_7);
                                break;
                            case "5":
                                btsheet.sheet(R.menu.delete_bottom_menu_for_7);
                                break;
                        }

                        btsheet.title("Operating " + contract_num_str)
                                .listener(new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case R.id.modify:
                                                Toast.makeText(mContext,
                                                        "Unable to modify deleted item", Toast.LENGTH_LONG).show();
                                                break;

                                            case R.id.undelete:

                                                DataBaseConnection connection =
                                                        new DataBaseConnection(Configure.URL_UNDELETE_CONTRACT,
                                                                Integer.parseInt(contract_num_str));
                                                connection.undelete_contract(adapter, ContractHolder.this);

                                                break;
                                            case R.id.print:
                                                System.out.println("print");
                                                break;
                                        }
                                    }
                                }).show();

                    }


                }
            });
        }

        public TextView getPhoneView() {
            return phone;
        }

        public TextView getFirst_nameView() {
            return first_name;
        }

        public TextView getLast_nameView() {
            return last_name;
        }

        public TextView getContract_amountView() {
            return contract_amount;
        }

        public TextView getReceipt_numView() {
            return receipt_num;
        }

        public TextView getContract_numView() {
            return contract_num;
        }

        public TextView getStaffView() {
            return staff;
        }

        public TextView getFile_idView() {
            return file_id;
        }

        public TextView getDateView() {
            return date;
        }

        public TextView getBranchView() {
            return branch;
        }

        public TextView getStatusView() {
            return status;
        }

        public TextView getBalanceView() {
            return balance;
        }

        public TextView getClassNameView() {
            return class_name;
        }

        public TextView getMemoView() {
            return memo;
        }

        public ImageView getArrowView() {
            return arrow;
        }

    }
}