package com.programmer1.customerbook.Fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.programmer1.customerbook.AaronSearch;
import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.R;
import com.programmer1.customerbook.ReceiptBook.AddReceipt;
import com.programmer1.customerbook.ReceiptBook.PrintReceipt;
import com.programmer1.customerbook.ReceiptBook.Receipt;
import com.programmer1.customerbook.ReceiptBook.ReceiptFragment;
import com.programmer1.customerbook.helper.SQLiteHandler;
import com.programmer1.customerbook.helper.SessionManager;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;
import static com.programmer1.customerbook.Configure.SaveReceiptToLocal;
import static com.programmer1.customerbook.Configure.URL_RECEIPT_HINT;

/**
 * Created by aaron on 4/2/2018.
 */

@SuppressLint("ValidFragment")
public class ReceiptBookFragment extends Fragment {

    private AaronSearch searchView;
    public static ArrayList<Receipt> search_result;
    private com.github.clans.fab.FloatingActionButton fab_contract;
    private static ReceiptFragment fragment;
    public static String right;

    private static final int RC_SEARCH = 1;
    private static final int INTERVAL = 500;
    //private EditText mEtHandler;
    private static Handler mHandler;
    private static String key = "";

    private String[] suggestion_results;

    private static Activity context = null;
    private static String search = "";

    private static ProgressDialog pDialog;


    @SuppressLint("HandlerLeak")
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
        }

        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        }


        View view = inflater.inflate(R.layout.activity_receipt, container,false);

        searchView= getActivity().findViewById(R.id.customer_searchView);
        context=getActivity();

        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);


        final SQLiteHandler db = new SQLiteHandler(context);
        final SessionManager session = new SessionManager(context);

        setHasOptionsMenu(true);

        HashMap<String, String> user = db.getUserDetails();
        right = user.get("right");

        fab_contract = view.findViewById(R.id.add_receipt);

        fab_contract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(),
                        AddReceipt.class);
                startActivityForResult(i,4);
            }
        });
//

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == RC_SEARCH) {
                    if(!key.equals("")){
                        System.out.println("searching"+ key);
                        getSearchSuggestions(URL_RECEIPT_HINT, key);
                    }

                }
            }
        };
        setSearch(searchView, Configure.URL_SEARCH_DEFAULT_RECEIPT, "Search Active Receipts");
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        searchView.setMenuItem(menu.findItem(R.id.search));
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {

            case R.id.active:
                setSearch(searchView, Configure.URL_SEARCH_DEFAULT_RECEIPT, "Search Active Receipts: Lastname Firstname");
                return true;
            case R.id.deleted:
                setSearch(searchView, Configure.URL_SEARCH_DELETED_RECEIPT, "Search Deleted Receipts: Lastname Firstname");
                return true;

            case R.id.print:
//                setSearch(searchView, Configure.URL_SEARCH_DELETED_CONTRACT, "Search Deleted Contracts");
                startActivity(new Intent(getActivity(), PrintReceipt.class));
                return true;

            default:
                return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 4) {
            if(resultCode == RESULT_OK) {
                try {
                    JSONObject Jobject = new JSONObject(data.getStringExtra("data"));
                    JSONArray Jarray = Jobject.getJSONArray("output");
                    SaveReceiptToLocal(getActivity(), Jarray.getJSONObject(0));
                    ReceiptBookFragment.stringToJson(data.getStringExtra("data"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ReceiptFragment fragment = new ReceiptFragment(search_result, right, getActivity());
                getActivity().getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.receipt_fragment_container, fragment)
                        .commit();
            }
        }
    }


    public static void setSearch(final AaronSearch searchView, final String url, final String hint) {

        if (searchView != null) {
            searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    search = query;
                    getSearchResults(url, query);
                    searchView.closeSearch();

                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    key = newText;
                    if (mHandler.hasMessages(RC_SEARCH)) {
                        mHandler.removeMessages(RC_SEARCH);
                    }
                    mHandler.sendEmptyMessageDelayed(RC_SEARCH, INTERVAL);

                    return true;
                }
            });
            searchView.setHint(hint);
        }
    }

    public static void getSearchResults(String url, String query){
        pDialog.setMessage("Searching " + query);
        showDialog();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        RequestBody form = new FormBody.Builder()
                .add("query", query)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             context.runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     hideDialog();
                                     Toast.makeText(context,
                                             "Search server disconnected", Toast.LENGTH_LONG).show();

                                 }
                             });
                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             String status = null;
                             JSONObject Jobject = null;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");

                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status != null ? status : "500";

                             context.runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     assert finalStatus != null;
                                     switch (finalStatus) {
                                         case "200":

                                             try {
                                                 stringToJson(res);
                                                 fragment = new ReceiptFragment(search_result, right, context);
                                                 context.getFragmentManager()
                                                         .beginTransaction()
                                                         .replace(R.id.receipt_fragment_container, fragment)
                                                         .commit();

                                             } catch (JSONException e) {
                                                 e.printStackTrace();
                                             }

                                             hideDialog();
                                             break;

                                         case "404":
                                             Toast.makeText(context,
                                                     "Receipt not found, enter correct name", Toast.LENGTH_LONG).show();
                                             if(fragment != null){
                                                 context.getFragmentManager().beginTransaction().remove(context.getFragmentManager().findFragmentById(R.id.receipt_fragment_container)).commit();
                                             }
                                             fragment = null;
                                             hideDialog();
                                             break;
                                         default:
                                             Toast.makeText(context,
                                                     "Server error, try again", Toast.LENGTH_LONG).show();
                                             hideDialog();
                                             break;
                                     }
                                 }
                             });
                         }
                     }
        );


    }

    public static void stringToJson(String str) throws JSONException {

        search_result = new ArrayList<>();
        JSONObject Jobject = new JSONObject(str);
        JSONArray Jarray = Jobject.getJSONArray("output");

        for (int i = 0; i < Jarray.length(); i++) {
            JSONObject object  = Jarray.getJSONObject(i);
            Receipt receipt = new Receipt(object.getString("date"),
                    object.getString("branch"),
                    object.getString("firstname"),
                    object.getString("lastname"),
                    object.getString("staff"),
                    object.getString("contract_num"),
                    object.getString("receipt_num"),
                    object.getString("amount"),
                    object.getString("balance"),
                    object.getString("refundable"),
                    object.getString("payment_method"),
                    object.getString("class"),
                    object.getString("deposit_where"),
                    object.getString("deposit_by"),
                    object.getString("deposit_date"),
                    object.getString("memo"),
                    object.getString("gst"),
                    object.getString("total_amount"),
                    object.getString("phone"),
                    object.getString("status")
            );
            search_result.add(receipt);
        }
    }





    public void addToSuggestionArray(String str) throws JSONException {

        ArrayList<String> suggestions= new ArrayList<>();
        JSONObject Jobject = new JSONObject(str);
        JSONArray Jarray = Jobject.getJSONArray("output");

        for (int i = 0; i < Jarray.length(); i++) {
            JSONObject object  = Jarray.getJSONObject(i);
            String hint = object.getString("hint");
            if (!suggestions.contains(hint)){
                suggestions.add(hint);
            }

        }
        suggestion_results = suggestions.toArray(new String[suggestions.size()]);
    }



    public void getSearchSuggestions(String url, String query){
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        RequestBody form = new FormBody.Builder()
                .add("query", query)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             getActivity().runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {

                                     Toast.makeText(getActivity(),
                                             "Suggestion Server disconnected", Toast.LENGTH_LONG).show();
                                 }
                             });
                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             System.out.println(res);
                             String status = null;
                             JSONObject Jobject = null;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");
                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status != null ? status : "500";

                             getActivity().runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     if(finalStatus != null){
                                         switch (finalStatus) {
                                             case "200":

                                                 try {
                                                     addToSuggestionArray(res);
                                                     searchView.setSuggestions(suggestion_results);
                                                     System.out.print(suggestion_results[0]);
                                                     System.out.println("suggestions set");


                                                 } catch (JSONException e) {
                                                     e.printStackTrace();
                                                 }


                                                 break;

                                             case "404":
                                                 Toast.makeText(getActivity(),
                                                         "Suggestions not found", Toast.LENGTH_LONG).show();


                                                 break;

                                             case "500":
                                                 Toast.makeText(getActivity(),
                                                         "Suggestions Server error, try again", Toast.LENGTH_LONG).show();
                                                 break;
                                         }
                                     }
                                 }
                             });
                         }
                     }
        );


    }

    private static void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private static void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
