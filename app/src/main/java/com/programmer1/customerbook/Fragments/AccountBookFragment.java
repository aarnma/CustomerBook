package com.programmer1.customerbook.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.programmer1.customerbook.AaronSearch;
import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.Login.RegisterActivity;
import com.programmer1.customerbook.MaintainAccount.Account;
import com.programmer1.customerbook.MaintainAccount.AccountFragment;
import com.programmer1.customerbook.R;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;



public class AccountBookFragment extends Fragment {

    private AaronSearch searchView;
    private static ArrayList<Account> search_result;
    private com.github.clans.fab.FloatingActionButton fab_contract;
    private static AccountFragment fragment;

    private static Activity context = null;
    private static ProgressDialog pDialog;

    @SuppressLint("HandlerLeak")
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_maintain, container,false);
        searchView= getActivity().findViewById(R.id.customer_searchView);
        context=getActivity();

        setHasOptionsMenu(true);

        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);

        fab_contract = view.findViewById(R.id.add_account);

        fab_contract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(),
                        RegisterActivity.class);
                startActivityForResult(i,7);
            }
        });

        setSearch(searchView, Configure.URL_SEARCH_DEFAULT, "Search active users");
        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        searchView.setMenuItem(menu.findItem(R.id.search));
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 7) {
            if(resultCode == RESULT_OK) {
                try {
                    stringToJson(data.getStringExtra("data"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                AccountFragment fragment = new AccountFragment(search_result, getActivity());
                getActivity().getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.account_fragment_container, fragment)
                        .commit();
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {

            case R.id.active:
                setSearch(searchView, Configure.URL_SEARCH_DEFAULT, "Search active users");
                return true;

            case R.id.deleted:
                setSearch(searchView, Configure.URL_SEARCH_DELETED, "Search deleted users");
                return true;
            default:
                return false;
        }
    }


    public static void setSearch(final AaronSearch searchView, final String url, final String hint) {

        if (searchView != null) {
            searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    getSearchResults(url, query);
                    searchView.closeSearch();
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });
            searchView.setHint(hint);
        }


    }


    public static void getSearchResults(String url, String name){

        pDialog.setMessage("Searching " + name);
        showDialog();

        OkHttpClient client = new OkHttpClient();

        RequestBody form = new FormBody.Builder()
                .add("name", name)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             context.runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     hideDialog();
                                     Toast.makeText(context,
                                             "Search server error", Toast.LENGTH_LONG).show();

                                 }
                             });
                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             hideDialog();

                             final String res = response.body().string();

                             String status = null;
                             JSONObject Jobject = null;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");
                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status != null ? status : "500";

                             context.runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     switch (finalStatus) {

                                         case "200":

                                             try {
                                                 stringToJson(res);
                                                 if (search_result.size() != 0) {
                                                     fragment = new AccountFragment(search_result, context);
                                                     context.getFragmentManager()
                                                             .beginTransaction()
                                                             .replace(R.id.account_fragment_container, fragment)
                                                             .commit();
                                                 }


                                             } catch (JSONException e) {
                                                 e.printStackTrace();
                                             }
                                             break;

                                         case "404":
                                             Toast.makeText(context,
                                                     "User not found", Toast.LENGTH_LONG).show();
                                             if(fragment!=null){
                                                 context.getFragmentManager().beginTransaction().remove(context.getFragmentManager().findFragmentById(R.id.account_fragment_container)).commit();
                                             }
                                             fragment = null;
                                             break;

                                         default:
                                             Toast.makeText(context,
                                                     "Server error, try again", Toast.LENGTH_LONG).show();
                                             break;
                                     }
                                 }
                             });
                         }
                     }
        );


    }

    public static void stringToJson(String str) throws JSONException {

        search_result = new ArrayList<>();
        JSONObject Jobject = new JSONObject(str);
        JSONArray Jarray = Jobject.getJSONArray("output");

        for (int i = 0; i < Jarray.length(); i++) {
            JSONObject object  = Jarray.getJSONObject(i);
            Account account = new Account(object.getString("name"),
                    object.getString("right"),  object.getString("status"));
            search_result.add(account);
        }
    }


    private static void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private static void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
