package com.programmer1.customerbook;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;

import com.programmer1.customerbook.Fragments.AccountBookFragment;
import com.programmer1.customerbook.Fragments.ContractBookFragment;
import com.programmer1.customerbook.Fragments.InBookFragment;
import com.programmer1.customerbook.Fragments.OutBookFragment;
import com.programmer1.customerbook.Fragments.ReceiptBookFragment;
import com.programmer1.customerbook.Login.LoginActivity;
import com.programmer1.customerbook.helper.BottomNavigationViewHelper;
import com.programmer1.customerbook.helper.SQLiteHandler;
import com.programmer1.customerbook.helper.SessionManager;

import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {

    private SQLiteHandler db;
    private SessionManager session;
    private FragmentManager fragmentManager;
//    private TextView title;
//    private AaronSearch searchView;
    private int index;
    private int currentTabIndex;
    private Fragment[] fragments;


    private ContractBookFragment contractBookFragment;
    private ReceiptBookFragment receiptBookFragment;
    private InBookFragment inBookFragment;
    private OutBookFragment outBookFragment;
    private AccountBookFragment accountBookFragment;

    private TextView title;
    private AaronSearch searchView;

    String[] empty = {""};

    private SweetAlertDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        HashMap<String, String> user = db.getUserDetails();
        String name = user.get("name");
        String right = user.get("right");

        BottomNavigationView navigation = findViewById(R.id.navigation);
        if(Integer.valueOf(right) < 9){
            navigation.getMenu().removeItem(R.id.navigation_account);
        }

        Toolbar toolbar = findViewById(R.id.customer_book_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        toolbar.setNavigationIcon(R.drawable.go_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessage();
            }
        });

        title = findViewById(R.id.customer_title);
        title.setText("Contracts");

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        fragmentManager = getSupportFragmentManager();

        contractBookFragment = new ContractBookFragment();
        receiptBookFragment = new ReceiptBookFragment();
        inBookFragment = new InBookFragment();
        outBookFragment = new OutBookFragment();
        accountBookFragment = new AccountBookFragment();
        fragments = new Fragment[]{contractBookFragment, receiptBookFragment, inBookFragment, outBookFragment, accountBookFragment};

        getSupportFragmentManager().beginTransaction().add(R.id.main_container, contractBookFragment).show(contractBookFragment)
                .commit();

        searchView = findViewById(R.id.customer_searchView);

    }

    private void showMessage() {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure to logout?")
                .setConfirmText("Yes")
                .setCancelText("No")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        logoutUser();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                String search = data.getStringExtra("key");
                ContractBookFragment.getSearchResults(Configure.URL_SEARCH_DEFAULT_CONTRACT, search);
            }
        }
        else if (requestCode == 2) {
            if(resultCode == RESULT_OK) {
                String search = data.getStringExtra("key");
                ReceiptBookFragment.getSearchResults(Configure.URL_SEARCH_DEFAULT_RECEIPT, search);
            }
        }
        else if (requestCode == 8) {
            if(resultCode == RESULT_OK) {
                String search = data.getStringExtra("key");
                AccountBookFragment.getSearchResults(Configure.URL_SEARCH_DEFAULT, search);
            }
        }
        else if (requestCode == 9) {
            if(resultCode == RESULT_OK) {
                String search = data.getStringExtra("key");
                InBookFragment.getSearchResults(Configure.URL_SEARCH_REF_IN, search);
            }
        }
        else if (requestCode == 10) {
            if(resultCode == RESULT_OK) {
                String search = data.getStringExtra("key");
                OutBookFragment.getSearchResults(Configure.URL_SEARCH_REF_OUT, search);
            }
        }

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_contract:
                    index = 0;
                    changFragment(index);
                    title.setText("Contracts");
                    ContractBookFragment.setSearch(searchView, Configure.URL_SEARCH_DEFAULT_CONTRACT, "Search Active Contracts");

                    break;
                case R.id.navigation_receipt:
                    index = 1;
                    changFragment(index);
                    title.setText("Receipts");
                    ReceiptBookFragment.setSearch(searchView, Configure.URL_SEARCH_DEFAULT_RECEIPT, "Search Active Receipts");

                    break;
                case R.id.navigation_in:
                    index = 2;
                    changFragment(index);
                    title.setText("In Mails");
                    InBookFragment.setSearch(searchView, Configure.URL_SEARCH_IN, "Search In Mail");

                    break;

                case R.id.navigation_out:
                    index = 3;
                    changFragment(index);
                    title.setText("Out Mails");
                    OutBookFragment.setSearch(searchView, Configure.URL_SEARCH_OUT, "Search Out Mail");

                    break;

                case R.id.navigation_account:
                    index = 4;
                    changFragment(index);
                    title.setText("Accounts");
                    AccountBookFragment.setSearch(searchView, Configure.URL_SEARCH_DEFAULT, "Search active users");

                    break;
            }
            return true;
        }
    };



    private void changFragment(int index) {

        if (currentTabIndex != index) {
            FragmentTransaction trx = getSupportFragmentManager().beginTransaction();
            trx.hide(fragments[currentTabIndex]);
            if (!fragments[index].isAdded()) {
                trx.add(R.id.main_container, fragments[index]);
            }
            trx.show(fragments[index]).commit();

        }
        currentTabIndex = index;
        searchView.setSuggestions(empty);
        if(searchView != null && searchView.isSearchOpen()){searchView.closeSearch();}

    }

    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }


}
