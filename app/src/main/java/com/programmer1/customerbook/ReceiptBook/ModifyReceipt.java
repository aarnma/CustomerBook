package com.programmer1.customerbook.ReceiptBook;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.Login.LoginActivity;
import com.programmer1.customerbook.R;
import com.programmer1.customerbook.helper.SQLiteHandler;
import com.programmer1.customerbook.helper.SessionManager;
import com.isapanah.awesomespinner.AwesomeSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by aaron on 1/31/2018.
 */

public class ModifyReceipt extends AppCompatActivity {

    private EditText input_firstname;
    private EditText input_lastname;
    private Button btnSave;
    private EditText inputContractNum;
    private EditText inputDepositDate;
    private EditText inputAmount;
    private AwesomeSpinner inputDepositBy;
    private SessionManager session;
    private SQLiteHandler db;
    private ProgressDialog pDialog;
    private HashMap<EditText, Boolean> editTextHashMap;
    private HashMap<AwesomeSpinner, Boolean> awesomeSpinnerHashMap;


    private EditText inputMemo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_receipt);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        // Session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        input_firstname = findViewById(R.id.modify_first_name);
        input_lastname = findViewById(R.id.modify_last_name);
        inputContractNum = findViewById(R.id.modify_contract_num);
        inputAmount= findViewById(R.id.receipt_amount);
        inputDepositBy = findViewById(R.id.receipt_deposit_by_textview_box);
        btnSave = findViewById(R.id.saveReceipt);

        inputMemo =  findViewById(R.id.modify_receipt_memo);

        inputDepositDate = findViewById(R.id.receipt_deposit_date);


        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        Toolbar toolbar = findViewById(R.id.modify_receipt_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        ArrayAdapter<CharSequence> depositbyAdapter = ArrayAdapter.createFromResource(this, R.array.staff_name, android.R.layout.simple_spinner_item);
        inputDepositBy.setAdapter(depositbyAdapter, 0);


        input_firstname.setText(getIntent().getStringExtra("receipt_firstname"));
        input_lastname.setText(getIntent().getStringExtra("receipt_lastname"));
        inputContractNum.setText(getIntent().getStringExtra("receipt_ContractNum"));
        inputAmount.setText(getIntent().getStringExtra("receipt_amount"));
        inputMemo.setText(getIntent().getStringExtra("receipt_memo"));

        awesomeSpinnerHashMap = new HashMap<>();
        awesomeSpinnerHashMap.put(inputDepositBy, false);


        inputDepositBy.setOnSpinnerItemClickListener(new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int position, String itemAtPosition) {
                //TODO YOUR ACTIONS
                awesomeSpinnerHashMap.put(inputDepositBy, true);
            }
        });



        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.CANADA);

                inputDepositDate.setText(sdf.format(myCalendar.getTime()));
            }

        };

        inputDepositDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(ModifyReceipt.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        editTextHashMap = new HashMap<>();
        editTextHashMap.put(inputContractNum, false);
        editTextHashMap.put(inputAmount, false);
        editTextHashMap.put(input_firstname, false);
        editTextHashMap.put(input_lastname, false);
        editTextHashMap.put(inputMemo, false);
        editTextHashMap.put(inputDepositDate, false);



        addTextChangeListener(inputContractNum, editTextHashMap);
        addTextChangeListener(inputAmount, editTextHashMap);
        addTextChangeListener(input_firstname, editTextHashMap);
        addTextChangeListener(input_lastname, editTextHashMap);
        addTextChangeListener(inputMemo, editTextHashMap);
        addTextChangeListener(inputDepositDate, editTextHashMap);


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HashMap<String, String> dataHashMap = new HashMap<>();
                String receipt_num = getIntent().getStringExtra("receipt_num");

                // Get a set of the entries
                Set set = editTextHashMap.entrySet();

                // Get an iterator
                Iterator i = set.iterator();

                // Display elements
                while(i.hasNext()) {
                    Map.Entry me = (Map.Entry)i.next();
                    EditText text = (EditText) me.getKey();

                    switch (text.getId()) {

                        case R.id.modify_first_name:

                            Boolean check = (Boolean) me.getValue();
                            if(check){
                                dataHashMap.put("firstname", input_firstname.getText().toString());
                            }
                            break;

                        case R.id.modify_last_name:

                           check = (Boolean) me.getValue();
                            if(check){
                                dataHashMap.put("lastname", input_lastname.getText().toString());
                            }
                            break;

                        case R.id.modify_contract_num:

                            check = (Boolean) me.getValue();
                            if(check){
                                dataHashMap.put("contract_num", inputContractNum.getText().toString());
                            }
                            break;

                        case R.id.receipt_amount:

                            check = (Boolean) me.getValue();
                            if(check){
                                double amount = Double.parseDouble(inputAmount.getText().toString());
                                double gst = amount * 0.05;
                                double total_amount = amount + gst;
                                dataHashMap.put("amount", String.valueOf(amount));
                                dataHashMap.put("GST", String.valueOf(gst));
                                dataHashMap.put("total_amount", String.valueOf(total_amount));
                            }
                            break;

                        case R.id.modify_receipt_memo:

                            check = (Boolean) me.getValue();
                            if(check){
                                dataHashMap.put("memo", inputMemo.getText().toString().replace("\n", "/m").trim());
                            }
                            break;

                        case R.id.receipt_deposit_date:

                            check = (Boolean) me.getValue();
                            if(check){
                                dataHashMap.put("deposit_date", inputDepositDate.getText().toString());
                            }
                            break;

                        default:
                            break;

                    }

                }

                Set spinner_set = awesomeSpinnerHashMap.entrySet();

                // Get an iterator
                Iterator spinner_iterator = spinner_set.iterator();

                // Display elements
                while(spinner_iterator.hasNext()) {
                    Map.Entry me = (Map.Entry)spinner_iterator.next();
                    AwesomeSpinner spinner = (AwesomeSpinner) me.getKey();

                    switch (spinner.getId()) {

                        case R.id.receipt_deposit_by_textview_box:
                            System.out.print( "Staff: ");
                            Boolean check = (Boolean) me.getValue();
                            if(check){
                                dataHashMap.put("deposit_by", inputDepositBy.getSelectedItem());
                            }
                            break;


                        default:
                            break;

                    }

                }

                if(dataHashMap.isEmpty()){
                    Toast.makeText(getApplicationContext(),
                            "Nothing changed", Toast.LENGTH_LONG).show();
                }
                else{
                    update(receipt_num, dataHashMap);
                }
            }
        });
    }


    private void update(final String receipt_num, HashMap<String, String> map) {

        pDialog.setMessage("Modifying ...");
        showDialog();


        Set set = map.entrySet();
        Iterator i = set.iterator();

        //chnage to int
        String json_str ="{\"receipt_num\": \"" + receipt_num +"\", " + "\"data\": [";
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue());
            json_str += "{\"" + me.getKey().toString() + "\": \"" + me.getValue().toString() + "\"}," ;
        }

        json_str=json_str.substring(0, json_str.length()-1)+"]}";
        System.out.println(json_str);

        final MediaType mediaType = MediaType.parse("application/json");

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        // GET request
        Request request = new Request.Builder()
                .url(Configure.URL_UPDATE_RECEIPT)
                .post(RequestBody.create(mediaType, json_str))
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        hideDialog();
                        Toast.makeText(getApplicationContext(),
                                "Cannot connect to server", Toast.LENGTH_LONG).show();

                    }
                });

                System.out.println(e);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                hideDialog();
                final String res = response.body().string();
                String status = null;
                JSONObject Jobject = null;
                String msg = null;
                try {
                    Jobject = new JSONObject(res);
                    status = Jobject.getString("status");
                    if (status.equals("404")){
                        msg = Jobject.getString("msg");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String finalStatus = status != null ? status : "500";
                final JSONObject finalJobject = Jobject;


                final String finalMsg = msg;
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        switch (finalStatus) {
                            case "200":
                                Toast.makeText(getApplicationContext(),
                                        "Modify successfully", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent();
                                intent.putExtra("key", input_lastname.getText().toString() + " "
                                + input_firstname.getText().toString());
                                setResult(RESULT_OK, intent);
                                finish();
                                break;

                            case "400":
                                Toast.makeText(getApplicationContext(),
                                        "Wrong data", Toast.LENGTH_LONG).show();
                                break;
                            case "404":
                                Toast.makeText(getApplicationContext(),
                                        finalMsg, Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(getApplicationContext(),
                                        "Server is busy, try again", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

        });

    }

    public void addTextChangeListener(final EditText input, final HashMap<EditText, Boolean> map){
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                map.put(input, true);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
