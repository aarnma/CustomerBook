package com.programmer1.customerbook.ReceiptBook;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.TextView;

import com.programmer1.customerbook.R;
import com.programmer1.customerbook.helper.SQLiteHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by aaron on 5/22/2018.
 */

public class DisplayReceiptResult extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_receipt);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Toolbar toolbar = findViewById(R.id.print_receipt_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        TextView from = findViewById(R.id.from_text);
        TextView to = findViewById(R.id.to_text);
        TextView count = findViewById(R.id.count_text);


        TextView cash = findViewById(R.id.cash_text);
        TextView cash_gst = findViewById(R.id.cash_gst_text);
        TextView cash_total = findViewById(R.id.cash_total_text);

        TextView cash_expense = findViewById(R.id.cash_expense_text);
        TextView cash_gst_expense = findViewById(R.id.cash_gst_expense_text);
        TextView cash_expense_total = findViewById(R.id.cash_expense_total_text);
        TextView cash_left = findViewById(R.id.cash_left_text);


        TextView total_revenue = findViewById(R.id.total_revenue_text);
        TextView total_expense = findViewById(R.id.total_expense_text);
        TextView total = findViewById(R.id.total_text);

        from.setText(getIntent().getStringExtra("from"));
        to.setText(getIntent().getStringExtra("to"));
        count.setText(getIntent().getStringExtra("count"));

        cash.setText(getIntent().getStringExtra("cash"));
        cash_gst.setText(getIntent().getStringExtra("cash_gst"));
        cash_total.setText(getIntent().getStringExtra("cash_total"));

        cash_expense.setText(getIntent().getStringExtra("cash_expense"));
        cash_gst_expense.setText(getIntent().getStringExtra("cash_gst_expense"));
        cash_expense_total.setText(getIntent().getStringExtra("cash_expense_total"));
        cash_left.setText(getIntent().getStringExtra("cash_left"));

        total_revenue.setText(getIntent().getStringExtra("total_revenue"));
        total_expense.setText(getIntent().getStringExtra("total_expense"));
        total.setText(getIntent().getStringExtra("total"));

        final SQLiteHandler db = new SQLiteHandler(getApplicationContext());
        HashMap<String, String> user = db.getUserDetails();
        String right = user.get("right");

        try {
            ReceiptFragment fragment = new ReceiptFragment(stringToJson(getIntent().getStringExtra("data")), right, DisplayReceiptResult.this);
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.result_container, fragment)
                    .commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public ArrayList<Receipt> stringToJson(String str) throws JSONException {

        ArrayList<Receipt> result = new ArrayList<>();
        JSONObject Jobject = new JSONObject(str);
        JSONArray Jarray = Jobject.getJSONArray("output");

        for (int i = 0; i < Jarray.length(); i++) {
            JSONObject object  = Jarray.getJSONObject(i);
            Receipt receipt = new Receipt(object.getString("date"),
                    object.getString("branch"),
                    object.getString("firstname"),
                    object.getString("lastname"),
                    object.getString("staff"),
                    object.getString("contract_num"),
                    object.getString("receipt_num"),
                    object.getString("amount"),
                    object.getString("balance"),
                    object.getString("refundable"),
                    object.getString("payment_method"),
                    object.getString("class"),
                    object.getString("deposit_where"),
                    object.getString("deposit_by"),
                    object.getString("deposit_date"),
                    object.getString("memo"),
                    object.getString("gst"),
                    object.getString("total_amount"),
                    object.getString("phone"),
                    object.getString("status")
            );
            result.add(receipt);
        }
        return result;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}