package com.programmer1.customerbook.ReceiptBook;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cocosw.bottomsheet.BottomSheet;
import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.Connection.DataBaseConnection;
import com.programmer1.customerbook.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aaron on 1/30/2018.
 */

public class ReceiptAdapter extends RecyclerView.Adapter<ReceiptAdapter.ReceiptHolder> {
    private Activity context;
    private List<Receipt> receipts;
    private String right;
    private BottomSheet.Builder btsheet;


    public ReceiptAdapter(Activity context, ArrayList<Receipt> receipts, String right) {
        this.context = context;
        this.receipts = receipts;
        this.right = right;

    }

    @Override
    public ReceiptAdapter.ReceiptHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ReceiptAdapter.ReceiptHolder(this, context,
                LayoutInflater.from(parent.getContext()).inflate(R.layout.receipt_layout, parent, false), right);
    }

    @Override
    public void onBindViewHolder(final ReceiptAdapter.ReceiptHolder holder, int position) {
        Receipt receipt = receipts.get(position);

        holder.getBranchView().setText(receipt.getBranch());
        holder.getFirst_nameView().setText(receipt.getFirst_name());
        holder.getLast_nameView().setText(receipt.getLast_name());
        holder.getAmountView().setText(receipt.getAmount());
        holder.getReceipt_numView().setText(receipt.getReceipt_num());
        holder.getContract_numView().setText(receipt.getContract_num());
        holder.getStaffView().setText(receipt.getStaff());
        holder.getDateView().setText(receipt.getDate());
        holder.getStatusView().setText(receipt.getStatus());

        holder.getRefundableView().setText(receipt.getRefundable());
        holder.getBalanceView().setText(receipt.getBalance());
        holder.getPayment_methodView().setText(receipt.getPayment_method());
        holder.getClassView().setText(receipt.getClass_str());
        holder.getDeposit_whereView().setText(receipt.getDeposit_where());
        holder.getDeposit_byView().setText(receipt.getDeposit_by());
        holder.getDeposit_dateView().setText(receipt.getDeposit_date());
        holder.getMemoView().setText(receipt.getMemo());
        holder.getGstView().setText(receipt.getGst());
        holder.getTotal_amountView().setText(receipt.getTotal_amount());

        holder.getPhoneView().setText(receipt.getPhone());

    }

    @Override
    public int getItemCount() {
        return receipts.size();
    }

    public void removeAt(int position) {
        receipts.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, receipts.size());
    }


    public class ReceiptHolder extends RecyclerView.ViewHolder {
        protected TextView date;
        protected TextView branch;
        protected TextView first_name;
        protected TextView last_name;
        protected TextView amount;
        protected TextView staff;
        protected TextView contract_num;
        protected TextView receipt_num;
        protected TextView status;
        private String right_str;
        protected ImageView arrow;
        protected Activity mContext;



        protected TextView balance;
        protected TextView refundable;
        protected TextView payment_method;
        protected TextView class_str;
        protected TextView deposit_where;
        protected TextView deposit_by;
        protected TextView deposit_date;
        protected TextView memo;
        protected TextView gst;
        protected TextView total_amount;

        protected TextView phone;

        public ReceiptHolder(final ReceiptAdapter adapter, final Activity context, View itemView, final String right) {
            super(itemView);
            this.right_str = right;
            date = itemView.findViewById(R.id.receipt_date_content);
            branch = itemView.findViewById(R.id.receipt_branch_content);
            first_name = itemView.findViewById(R.id.receipt_first_name_content);
            last_name = itemView.findViewById(R.id.receipt_last_name_content);
            staff = itemView.findViewById(R.id.receipt_staff_content);
            contract_num = itemView.findViewById(R.id.receipt_contract_id_content);
            receipt_num  = itemView.findViewById(R.id.receipt_num_content);
            status = itemView.findViewById(R.id.receipt_status_content);
            amount = itemView.findViewById(R.id.receipt_amount_content);

            arrow = itemView.findViewById(R.id.receipt_arrow_icon);
            mContext = context;


            balance = itemView.findViewById(R.id.receipt_balance_content);
            refundable = itemView.findViewById(R.id.receipt_refundable_content);
            payment_method = itemView.findViewById(R.id.receipt_pay_method_content);
            class_str = itemView.findViewById(R.id.receipt_class_content);
            deposit_where = itemView.findViewById(R.id.receipt_deposit_where_content);
            deposit_by = itemView.findViewById(R.id.receipt_deposit_by_content);
            deposit_date  = itemView.findViewById(R.id.receipt_deposit_date_content);
            memo = itemView.findViewById(R.id.receipt_memo_content);
            gst = itemView.findViewById(R.id.receipt_gst_content);
            total_amount = itemView.findViewById(R.id.receipt_total_amount_content);

            phone= itemView.findViewById(R.id.receipt_phone_content);

            arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String receipt_num_text = receipt_num.getText().toString();
                    final String status_str = status.getText().toString();

                    final String contract_num_str = contract_num.getText().toString();
                    final String lastname_str = last_name.getText().toString();
                    final String firstname_str = first_name.getText().toString();
                    final String amount_str = amount.getText().toString();
                    final String receipt_memo_text = memo.getText().toString();

                    btsheet = new BottomSheet.Builder(mContext);

                    String account_right = (right_str != null) ? right_str : "5";

                    if(status_str.equals("a")){

                        switch (account_right){
                            case "9":
                                btsheet.sheet(R.menu.bottom_menu);
                                break;
                            case  "7":
                                btsheet.sheet(R.menu.bottom_menu_for_7);
                                break;
                            case "5":
                                btsheet.sheet(R.menu.bottom_menu_for_5);
                                break;
                        }

                        btsheet.title("Operating " + receipt_num_text)
                                .listener(new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case R.id.modify:

                                                System.out.println("Modify");
                                                Intent intent = new Intent(context, ModifyReceipt.class);
                                                intent.putExtra("receipt_firstname", firstname_str);
                                                intent.putExtra("receipt_lastname", lastname_str);
                                                intent.putExtra("receipt_ContractNum", contract_num_str);
                                                intent.putExtra("receipt_amount", amount_str);
                                                intent.putExtra("receipt_num", receipt_num_text);
                                                intent.putExtra("receipt_memo", receipt_memo_text);
                                                ((Activity) mContext).startActivityForResult(intent,2);

                                                break;
                                            case R.id.delete:
                                                System.out.println("delete");
                                                DataBaseConnection connection =
                                                        new DataBaseConnection(Configure.URL_DELETE_RECEIPT,
                                                                Integer.parseInt(receipt_num_text));
                                                connection.delete_receipt(adapter, ReceiptAdapter.ReceiptHolder.this);
                                                break;
                                            case R.id.print:
                                                System.out.println("print");
                                                break;
                                        }
                                    }
                                }).show();
                    }
                    else if(status_str.equals("d")){

                        switch (account_right){
                            case "9":
                                btsheet.sheet(R.menu.deleted_bottom_menu);
                                break;
                            case  "7":
                                btsheet.sheet(R.menu.delete_bottom_menu_for_7);
                                break;
                            case "5":
                                btsheet.sheet(R.menu.delete_bottom_menu_for_7);
                                break;
                        }

                        btsheet.title("Operating " + receipt_num_text)
                                .listener(new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {

                                            case R.id.undelete:

                                                DataBaseConnection connection =
                                                        new DataBaseConnection(Configure.URL_UNDELETE_RECEIPT,
                                                                Integer.parseInt(receipt_num_text));
                                                connection.undelete_receipt(adapter, ReceiptAdapter.ReceiptHolder.this);

                                                break;
                                            case R.id.print:
                                                System.out.println("print");
                                                break;
                                        }
                                    }
                                }).show();

                    }


                }
            });
        }

        public TextView getFirst_nameView() {
            return first_name;
        }

        public TextView getLast_nameView() {
            return last_name;
        }

        public TextView getAmountView() {return amount;}

        public TextView getReceipt_numView() {
            return receipt_num;
        }

        public TextView getContract_numView() {
            return contract_num;
        }

        public TextView getStaffView() {
            return staff;
        }

        public TextView getDateView() {
            return date;
        }

        public TextView getBranchView() {
            return branch;
        }

        public TextView getStatusView() {
            return status;
        }


        public TextView getBalanceView() {
            return balance;
        }

        public TextView getRefundableView() {
            return refundable;
        }

        public TextView getPayment_methodView() {return payment_method;}

        public TextView getClassView() {
            return class_str;
        }

        public TextView getDeposit_whereView() {
            return deposit_where;
        }

        public TextView getDeposit_byView() {
            return deposit_by;
        }

        public TextView getDeposit_dateView() {
            return deposit_date;
        }

        public TextView getMemoView() {
            return memo;
        }

        public TextView getGstView() {
            return gst;
        }

        public TextView getTotal_amountView() {
            return total_amount;
        }

        public TextView getPhoneView() {
            return phone;
        }

    }
}
