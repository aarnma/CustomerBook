package com.programmer1.customerbook.ReceiptBook;

/**
 * Created by Programmer 1 on 2018-02-14.
 */

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.isapanah.awesomespinner.R.id;
import com.isapanah.awesomespinner.R.layout;
import com.isapanah.awesomespinner.R.styleable;
import com.isapanah.awesomespinner.spinnerDefaultSelection;

public class AaronSpinner extends RelativeLayout {
    private static final String TAG = AaronSpinner.class.getSimpleName();
    private AppCompatButton _hintButton;
    private spinnerDefaultSelection _spinner;
    private ImageView _downArrow;
    private ArrayAdapter<String> _spinnerAdapterString;
    private ArrayAdapter<CharSequence> _spinnerAdapterCharSequence;
    private boolean _allowToSelect;
    private AaronSpinner.onSpinnerItemClickListener<String> _callback;
    private boolean _isItemResourceDeclared = false;
    private int _spinnerType = 0;
    private boolean _isSelected;

    public AaronSpinner(Context context) {
        super(context);
        this.init((AttributeSet) null);
    }

    public AaronSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(attrs);
    }

    public AaronSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.init(attrs);
    }

    private void init(AttributeSet attrs) {
        inflate(this.getContext(), layout.spinner_view, this);
        this._hintButton = (AppCompatButton) this.findViewById(id.awesomeSpinner_hintButton);
        this._spinner = (spinnerDefaultSelection) this.findViewById(id.awesomeSpinner_spinner);
        this._downArrow = (ImageView) this.findViewById(id.awesomeSpinner_downArrow);
        if (attrs != null) {
            this.setSpinnerStyle(this.getContext().obtainStyledAttributes(attrs, styleable.AwesomeSpinnerStyle, 0, 0));
        }

    }

    private void setSpinnerStyle(TypedArray typedArray) {
        this._hintButton.setText(typedArray.getString(styleable.AwesomeSpinnerStyle_spinnerHint));
        LayoutParams params = new LayoutParams(-2, -2);
        params.setMargins(10, 40, 10, 10);
        switch (typedArray.getInt(styleable.AwesomeSpinnerStyle_spinnerDirection, 0)) {
            case 0:
                this._hintButton.setGravity(19);
                params.addRule(11);
                this._downArrow.setLayoutParams(params);
                break;
            case 1:
                this._hintButton.setGravity(21);
                params.addRule(9);
                this._downArrow.setLayoutParams(params);
        }

    }

    public void setAdapter(ArrayAdapter<String> adapter) {
        this._spinnerAdapterString = adapter;
        this._spinner.setAdapter(this._spinnerAdapterString);
        this.initiateSpinnerString();
    }

    public void setAdapter(ArrayAdapter<CharSequence> adapter, int idle) {
        this._spinnerType = 1;
        this._spinnerAdapterCharSequence = adapter;
        this._spinner.setAdapter(this._spinnerAdapterCharSequence);
        this.initiateSpinnerCharSequence();
    }

    public boolean isSelected() {
        return this._isSelected;
    }

    private void initiateSpinnerString() {
        if (!this._isItemResourceDeclared) {
            this._spinnerAdapterString.setDropDownViewResource(layout.spinner_list_item);
        }

        this._spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(AaronSpinner.TAG, "position selected: " + position);
                if (AaronSpinner.this._callback == null) {
                    throw new IllegalStateException("callback cannot be null");
                } else {
                    if (AaronSpinner.this._allowToSelect) {
                        Object item = AaronSpinner.this._spinner.getItemAtPosition(position);
                        AaronSpinner.this._callback.onItemSelected(position, (String) item);
                        AaronSpinner.this._hintButton.setText(AaronSpinner.this._spinner.getSelectedItem().toString());
                        AaronSpinner.this._hintButton.setTextColor(-16777216);
                        AaronSpinner.this._isSelected = true;
                    }

                    AaronSpinner.this._allowToSelect = true;
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(AaronSpinner.TAG, "Nothing selected");
            }
        });
    }

    private void initiateSpinnerCharSequence() {
        if (!this._isItemResourceDeclared) {
            this._spinnerAdapterCharSequence.setDropDownViewResource(layout.spinner_list_item);
        }

        this._spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(AaronSpinner.TAG, "position selected: " + position);
                if (AaronSpinner.this._callback == null) {
                    throw new IllegalStateException("callback cannot be null");
                } else {
                    if (AaronSpinner.this._allowToSelect) {
                        Object item = AaronSpinner.this._spinner.getItemAtPosition(position);
                        AaronSpinner.this._callback.onItemSelected(position, (String) item);
                        AaronSpinner.this._hintButton.setText(AaronSpinner.this._spinner.getSelectedItem().toString());
                        AaronSpinner.this._hintButton.setTextColor(-16777216);
                        AaronSpinner.this._isSelected = true;
                    }

                    AaronSpinner.this._allowToSelect = true;
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(AaronSpinner.TAG, "Nothing selected");
            }
        });
    }

    public void setHint(String hint) {
        AaronSpinner.this._hintButton.setText(hint);
        AaronSpinner.this._hintButton.setTextColor(Color.GRAY);
        AaronSpinner.this._isSelected = false;
    }

    public void setDropDownViewResource(int resource) {
        if (this._spinnerType == 1) {
            this._spinnerAdapterCharSequence.setDropDownViewResource(resource);
        } else {
            this._spinnerAdapterString.setDropDownViewResource(resource);
        }

        this._isItemResourceDeclared = true;
    }

    public void setOnSpinnerItemClickListener(AaronSpinner.onSpinnerItemClickListener<String> callback) {
        this._callback = callback;
        this._hintButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                AaronSpinner.this._spinner.performClick();
            }
        });
    }

    public void setSelection(int position) {
        this._allowToSelect = true;
        this._spinner.setSelection(position);
    }

    public void setSelection(String value) {
        if (value.trim().isEmpty()) {
            this._allowToSelect = true;
            int spinnerPosition;
            if (this._spinnerType == 0) {
                spinnerPosition = this._spinnerAdapterString.getPosition(value);
                this._spinner.setSelection(spinnerPosition);
            } else {
                spinnerPosition = this._spinnerAdapterCharSequence.getPosition(value);
                this._spinner.setSelection(spinnerPosition);
            }
        }

    }

    public String getSelectedItem() {
        return this.isSelected() ? this._spinner.getSelectedItem().toString() : null;
    }

    public spinnerDefaultSelection getSpinner() {
        return this._spinner;
    }

    public int getSelectedItemPosition() {
        return this._spinner.getSelectedItemPosition();
    }

    public interface onSpinnerItemClickListener<T> {
        void onItemSelected(int var1, T var2);
    }
}
