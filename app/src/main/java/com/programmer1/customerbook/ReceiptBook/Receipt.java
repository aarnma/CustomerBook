package com.programmer1.customerbook.ReceiptBook;

/**
 * Created by aaron on 1/29/2018.
 */

public class Receipt {

    private String date;
    private String branch;
    private String first_name;
    private String last_name;
    private String staff;
    private String contract_num;
    private String receipt_num;
    private String amount;
    private String status;

    private String balance;
    private String refundable;
    private String payment_method;
    private String class_str;
    private String deposit_where;
    private String deposit_by;
    private String deposit_date;
    private String memo;
    private String gst;
    private String total_amount;
    private String phone;

    public Receipt(String date, String branch, String first_name, String last_name,
                   String staff, String contract_num, String receipt_num, String amount,

                   String balance, String refundable, String payment_method, String class_str,
                   String deposit_where, String deposit_by, String deposit_date, String memo,
                   String gst, String total_amount,String phone,

                   String status) {
        this.date = date;
        this.branch = branch;
        this.first_name = first_name;
        this.last_name = last_name;
        this.staff = staff;
        this.contract_num = contract_num;
        this.receipt_num = receipt_num;
        this.amount = amount;
        this.status = status;

        this.balance = balance;
        this.refundable = refundable;
        this.payment_method = payment_method;
        this.class_str = class_str;
        this.deposit_where = deposit_where;
        this.deposit_by = deposit_by;
        this.deposit_date = deposit_date;
        this.memo = memo;
        this.gst = gst;
        this.total_amount = total_amount;
        this.phone = phone;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getBranch() {
        return branch;
    }
    public void setBranch(String branch) {
        this.branch = branch;
    }
    public String getFirst_name() {
        return first_name;
    }
    public String getLast_name() {
        return last_name;
    }
    public String getContract_num() {
        return contract_num;
    }
    public String getReceipt_num() {
        return receipt_num;
    }
    public String getAmount() {return amount;}
    public String getStaff() {
        return staff;
    }
    public String getStatus() {
        return status;
    }

    public String getBalance() {
        return balance;
    }
    public String getRefundable() {
        return refundable;
    }
    public String getPayment_method() {
        return payment_method;
    }
    public String getClass_str() {
        return class_str;
    }
    public String getDeposit_where() {
        return deposit_where;
    }
    public String getDeposit_by() {
        return deposit_by;
    }
    public String getDeposit_date() {return deposit_date;}
    public String getMemo() {
        return memo;
    }
    public String getGst() {
        return gst;
    }
    public String getTotal_amount() {
        return total_amount;
    }
    public String getPhone() {
        return phone;
    }
}
