package com.programmer1.customerbook.ReceiptBook;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.ContractBook.Balance;
import com.programmer1.customerbook.Login.LoginActivity;
import com.programmer1.customerbook.R;
import com.programmer1.customerbook.helper.SQLiteHandler;
import com.programmer1.customerbook.helper.SessionManager;
import com.isapanah.awesomespinner.AwesomeSpinner;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by aaron on 1/29/2018.
 */

public class AddReceipt extends AppCompatActivity {

    private EditText inputDepositDate;
    private EditText inputMemo;
    private AwesomeSpinner refundable;
    private AwesomeSpinner payment_method;

    private AwesomeSpinner deposit_where;
    private AwesomeSpinner deposit_by;

    private EditText inputFirstName;
    private EditText inputLastName;

    private EditText inputAmount;
    private EditText inputGst;
    private AwesomeSpinner staff;
    private AwesomeSpinner branch;

    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;
    private String branch_id="";

    private AaronSpinner inputContractNum;

    private ArrayList<String> contract_num_list;
    private ArrayAdapter<String> contractnumAdapter;
    private AVLoadingIndicatorView loading;
    private TextView contract_num_msg;
    private TextView balance_msg;

    private AaronSpinner inputPhoneNum;
    private AVLoadingIndicatorView loading_phone;
    private TextView phone_num_msg;
    private ArrayList<String> phone_num_list;
    private ArrayAdapter<String> phonenumAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_receipt);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        inputFirstName = findViewById(R.id.first_name_receipt);
        inputLastName = findViewById(R.id.last_name_receipt);
        inputContractNum = findViewById(R.id.contract_num);
        inputAmount = findViewById(R.id.receipt_amount);
        staff = findViewById(R.id.select_staff_receipt);
        branch = findViewById(R.id.select_branch_receipt);
        Button add_receipt = findViewById(R.id.btnAddReceipt);
        inputDepositDate = findViewById(R.id.deposit_date);
        inputMemo = findViewById(R.id.receipt_memo);
        refundable = findViewById(R.id.refundable_status);
        payment_method = findViewById(R.id.payment_method);

        deposit_where = findViewById(R.id.where_deposit);
        deposit_by = findViewById(R.id.deposit_staff);
        loading  = findViewById(R.id.avi);
        contract_num_msg = findViewById(R.id.contract_num_msg);
        Button get_balance = findViewById(R.id.load_balance);
        balance_msg = findViewById(R.id.balance_detail);

        inputPhoneNum = findViewById(R.id.contract_phone_num);
        loading_phone  = findViewById(R.id.avi_phone_num);
        phone_num_msg = findViewById(R.id.contract_phone_num_msg);

        inputGst = findViewById(R.id.receipt_gst);

        Toolbar toolbar = findViewById(R.id.add_receipt_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");


        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        AwesomeSpinner.onSpinnerItemClickListener listener = new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int position, String itemAtPosition) {
            }
        };

        ArrayAdapter<CharSequence> staffAdapter = ArrayAdapter.createFromResource(this, R.array.staff_name, android.R.layout.simple_spinner_item);
        staff.setAdapter(staffAdapter, 0);

        ArrayAdapter<CharSequence> branchAdapter = ArrayAdapter.createFromResource(this, R.array.branch_name, android.R.layout.simple_spinner_item);
        branch.setAdapter(branchAdapter, 0);

        ArrayAdapter<CharSequence> refundableAdapter = ArrayAdapter.createFromResource(this, R.array.refundable_name, android.R.layout.simple_spinner_item);
        refundable.setAdapter(refundableAdapter, 0);

        ArrayAdapter<CharSequence> paymentMethodAdapter = ArrayAdapter.createFromResource(this, R.array.payment_method, android.R.layout.simple_spinner_item);
        payment_method.setAdapter(paymentMethodAdapter, 0);


        ArrayAdapter<CharSequence> depositWhereAdapter = ArrayAdapter.createFromResource(this, R.array.where_deposit_name, android.R.layout.simple_spinner_item);
        deposit_where.setAdapter(depositWhereAdapter, 0);

        ArrayAdapter<CharSequence> depositByAdapter = ArrayAdapter.createFromResource(this, R.array.staff_name, android.R.layout.simple_spinner_item);
        deposit_by.setAdapter(depositByAdapter, 0);

        staff.setOnSpinnerItemClickListener(listener);
        deposit_by.setOnSpinnerItemClickListener(listener);
        refundable.setOnSpinnerItemClickListener(listener);
        payment_method.setOnSpinnerItemClickListener(listener);

        deposit_where.setOnSpinnerItemClickListener(listener);


        View.OnFocusChangeListener foucs_listener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(!hasFocus){
                    String firstname = inputFirstName.getText().toString();
                    String lastname = inputLastName.getText().toString();
                    if(!firstname.isEmpty() && !lastname.isEmpty()){
                        loading.setVisibility(View.VISIBLE);
                        loading.show();
                        contract_num_msg.setVisibility(View.VISIBLE);
                        contract_num_msg.setText("Loading Contract Number");

                        loading_phone.setVisibility(View.VISIBLE);
                        loading_phone.show();
                        phone_num_msg.setVisibility(View.VISIBLE);
                        phone_num_msg.setText("Loading Phone Number");

                        getNums(Configure.URL_SEARCH_CONTRACT_NUM, firstname, lastname, "contract");
                        getNums(Configure.URL_SEARCH_PHONE_NUM, firstname, lastname, "phone");

                    }
                }}
        };

       inputFirstName.setOnFocusChangeListener(foucs_listener);
       inputLastName.setOnFocusChangeListener(foucs_listener);


        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.CANADA);

                inputDepositDate.setText(sdf.format(myCalendar.getTime()));
            }

        };

        inputDepositDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(AddReceipt.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        branch.setOnSpinnerItemClickListener(new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int i, String s) {
                if(s.equals("RichMond HQ")){
                    branch_id = "01";

                }
                else if(s.equals("Burnaby Crystal Mall")){
                    branch_id = "02";
                }
            }
        });

        get_balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String amount = inputAmount.getText().toString();
                String contract_num = inputContractNum.getSelectedItem();
                if(!amount.isEmpty() && inputContractNum.isSelected()){
                    Balance balance = new Balance(Configure.URL_GET_BALANCE);
                    balance.setBalanceHelper(contract_num, amount, balance_msg);
                }
                else{
                    balance_msg.setText("Please enter amount and contract #");
                }
            }
        });

        add_receipt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String firstname = inputFirstName.getText().toString().trim();
                String lastname = inputLastName.getText().toString().trim();
                String amount = inputAmount.getText().toString().trim();
                String gst = inputGst.getText().toString().trim();

                String contract_num = inputContractNum.getSelectedItem();
                String staff_name = staff.getSelectedItem();
                String branch_name = branch.getSelectedItem();
                String depositDate = inputDepositDate.getText().toString().trim();
                String memo = inputMemo.getText().toString();

                String refundable_str = refundable.getSelectedItem();
                String payment_method_str = payment_method.getSelectedItem();

                String deposit_where_str = deposit_where.getSelectedItem();
                String deposit_by_str = deposit_by.getSelectedItem() == null ? "" : deposit_by.getSelectedItem();
                String phone_str = inputPhoneNum.getSelectedItem() == null ? "" : inputPhoneNum.getSelectedItem();


                if (!firstname.isEmpty() && !lastname.isEmpty() && !amount.isEmpty() && !gst.isEmpty()&&
                        !contract_num.isEmpty() && staff.isSelected() && branch.isSelected()
                        && refundable.isSelected()&& payment_method.isSelected()
                        && deposit_where.isSelected()
                        && !branch_id.isEmpty()) {
                    add_receipt_to_database(firstname, lastname, amount, contract_num, staff_name,
                            branch_name, branch_id, refundable_str, payment_method_str,
                            deposit_where_str, deposit_by_str, depositDate, memo, phone_str, gst);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please fill up all fields", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
    }

    private void add_receipt_to_database(final String firstname, final String lastname, final String amount,
                                         final String contract_num, final String staff_name,
                                         final String branch_name, final String branch_id, final String refundable_str,
                                         final String payment_method_str,final String deposit_where_str,
                                         final String deposit_by_str, final String depositDate, final String memo, final String phone, final String gst) {

        pDialog.setMessage("Adding ...");
        showDialog();

        RequestBody form = new FormBody.Builder()
                .add("firstname", firstname)
                .add("lastname", lastname)
                .add("amount", amount)
                .add("contract_num", contract_num)
                .add("staff_name", staff_name)
                .add("branch_id", branch_id)
                .add("branch_name", branch_name)
                .add("refundable", refundable_str)
                .add("payment_method", payment_method_str)
                .add("deposit_where", deposit_where_str)
                .add("deposit_by", deposit_by_str)
                .add("deposit_date", depositDate)
                .add("memo", memo)
                .add("phone", phone)
                .add("gst", gst)
                .build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        Request request = new Request.Builder()
                .url(Configure.URL_ADD_RECEIPT)
                .post(form)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        hideDialog();
                        Toast.makeText(getApplicationContext(),
                                "Server is disconnected", Toast.LENGTH_LONG).show();

                    }
                });

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String res = response.body().string();

                String status = null;
                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(res);
                    status = Jobject.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String finalStatus = status != null ? status : "500";

                hideDialog();
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        switch (finalStatus) {
                            case "200":
                                Toast.makeText(getApplicationContext(),
                                        "New receipt added", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent();
                                intent.putExtra("data", res);
                                setResult(RESULT_OK, intent);
                                finish();
                                break;

                            case "404":
                                Toast.makeText(getApplicationContext(),
                                        "Wrong Contract Number or First/Last Name", Toast.LENGTH_LONG).show();
                                break;

                            case "500":
                                Toast.makeText(getApplicationContext(),
                                        "Server is busy", Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(getApplicationContext(),
                                        "Server is disconnected", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

        });

    }


    public void getNums(String url, String firstname, String lastname, final String type){


        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        RequestBody form = new FormBody.Builder()
                .add("firstname", firstname)
                .add("lastname", lastname)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {


                                     switch (type){
                                         case "phone":
                                             loading_phone.hide();
                                             phone_num_msg.setTextColor(Color.parseColor("#DC143C"));
                                             phone_num_msg.setText("Server down");
                                             break;
                                         case "contract":
                                             loading.hide();
                                             contract_num_msg.setTextColor(Color.parseColor("#DC143C"));
                                             contract_num_msg.setText("Server down");
                                             break;
                                     }

                                 }
                             });
                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             String status = null;
                             JSONObject Jobject = null;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");

                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status != null ? status : "500";

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     assert finalStatus != null;
                                     switch (finalStatus) {
                                         case "200":
                                             try {
                                                 if(type.equals("phone")){
                                                     phone_num_list = new ArrayList<>();
                                                     stringToJson(res, phone_num_list, "phone");
                                                     phonenumAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_dropdown_item, phone_num_list);
                                                     inputPhoneNum.setAdapter(phonenumAdapter);
                                                     inputPhoneNum.setDropDownViewResource(R.layout.spinner_dropdown_item);
                                                     inputPhoneNum.setOnSpinnerItemClickListener(new AaronSpinner.onSpinnerItemClickListener<String>() {
                                                         @Override
                                                         public void onItemSelected(int var1, String var2) {

                                                         }
                                                     });
                                                     loading_phone.hide();
                                                     phone_num_msg.setTextColor(Color.parseColor("#26ae90"));
                                                     phone_num_msg.setText("Successful, click to select!");
                                                 }
                                                 else if(type.equals("contract")){
                                                     contract_num_list = new ArrayList<>();
                                                     stringToJson(res, contract_num_list, "contract_num");
                                                     contractnumAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_dropdown_item, contract_num_list);
                                                     inputContractNum.setAdapter(contractnumAdapter);
                                                     inputContractNum.setDropDownViewResource(R.layout.spinner_dropdown_item);
                                                     inputContractNum.setOnSpinnerItemClickListener(new AaronSpinner.onSpinnerItemClickListener<String>() {
                                                         @Override
                                                         public void onItemSelected(int var1, String var2) {

                                                         }
                                                     });
                                                     loading.hide();
                                                     contract_num_msg.setTextColor(Color.parseColor("#26ae90"));
                                                     contract_num_msg.setText("Successful, click to select!");

                                                 }

                                             } catch (JSONException e) {
                                                 e.printStackTrace();
                                             }

                                             break;

                                         case "404":
                                             if(type.equals("phone")){
                                                 if(phone_num_list != null){
                                                     inputPhoneNum.setAdapter(null);
                                                 }
                                                 inputPhoneNum.setHint("Contract Num");
                                                 loading_phone.hide();
                                                 phone_num_msg.setTextColor(Color.parseColor("#DC143C"));
                                                 phone_num_msg.setText("No found, name is wrong!");
                                             }
                                             else if(type.equals("contract")){
                                                 if(contract_num_list != null){
                                                     inputContractNum.setAdapter(null);
                                                 }
                                                 inputContractNum.setHint("Contract Num");
                                                 loading.hide();
                                                 contract_num_msg.setTextColor(Color.parseColor("#DC143C"));
                                                 contract_num_msg.setText("No found, name is wrong!");
                                             }
                                             break;
                                         default:
                                             if(type.equals("phone")){
                                                 loading_phone.hide();
                                                 phone_num_msg.setTextColor(Color.parseColor("#DC143C"));
                                                 phone_num_msg.setText("Server error, try again");
                                             }
                                             else if(type.equals("contract")){
                                                 loading.hide();
                                                 contract_num_msg.setTextColor(Color.parseColor("#DC143C"));
                                                 contract_num_msg.setText("Server error, try again");
                                             }
                                             break;
                                     }
                                 }
                             });
                         }
                     }
        );


    }


    public static void stringToJson(String str, ArrayList<String> list, String num) throws JSONException {

        JSONObject Jobject = new JSONObject(str);
        JSONArray Jarray = Jobject.getJSONArray("output");

        for (int i = 0; i < Jarray.length(); i++) {
            JSONObject object  = Jarray.getJSONObject(i);
            list.add(object.getString(num));
        }
    }



    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
