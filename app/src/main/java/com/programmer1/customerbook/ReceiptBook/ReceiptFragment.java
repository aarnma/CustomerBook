package com.programmer1.customerbook.ReceiptBook;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.programmer1.customerbook.R;

import java.util.ArrayList;

/**
 * Created by aaron on 1/30/2018.
 */

public class ReceiptFragment extends Fragment {

    ArrayList<Receipt> receipts;
    String right;
    Context context;

    public ReceiptFragment(){}

    @SuppressLint("ValidFragment")
    public ReceiptFragment(ArrayList<Receipt> receipts, String right, Context context){
        this.receipts = receipts;
        this.right = right;
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recycler_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        RecyclerView rc = view.findViewById(R.id.list);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider_larger));
        rc.addItemDecoration(itemDecorator);

        ReceiptAdapter adapter = new ReceiptAdapter(getActivity(), receipts, right);
        rc.setLayoutManager(new LinearLayoutManager(context));
        rc.setHasFixedSize(true);
        rc.setAdapter(adapter);

    }
}
