package com.programmer1.customerbook.MaintainAccount;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cocosw.bottomsheet.BottomSheet;
import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.Connection.DataBaseConnection;
import com.programmer1.customerbook.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aaron on 1/23/2018.
 */

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.AccountHolder> {
    private Activity context;
    private List<Account> accounts;

    private BottomSheet sheet;


    public AccountAdapter(Activity context, ArrayList<Account> accounts) {
        this.context = context;
        this.accounts = accounts;

    }

    @Override
    public AccountAdapter.AccountHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AccountAdapter.AccountHolder(this, context, LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(final AccountAdapter.AccountHolder holder, int position) {
        Account account = accounts.get(position);
        holder.getNameView().setText(account.getName());
        holder.getRightView().setText(account.getRight());
        holder.getStatusView().setText(account.getStatus());

    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }

    public void removeAt(int position) {
        accounts.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, accounts.size());
    }



    public class AccountHolder extends RecyclerView.ViewHolder{
        protected TextView name;
        protected TextView right;
        protected TextView status;
        protected ImageView arrow;
        protected Activity mContext;

        public AccountHolder(final AccountAdapter adapter, final Activity context, View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.profile_name_content);
            right = itemView.findViewById(R.id.profile_right_content);
            status = itemView.findViewById(R.id.profile_status_content);
            arrow = itemView.findViewById(R.id.arrow_icon);
            mContext = context;


            arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String name_str = name.getText().toString();
                    final String status_str = status.getText().toString();
                    final String right_str = right.getText().toString();
                    System.out.println(name_str);
                    System.out.println(status_str);

                    if(status_str.equals("a")){
                        sheet = new BottomSheet.Builder(mContext)
                                .sheet(R.menu.bottom_menu)
                                .title("Operating " + name_str)
                                .listener(new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case R.id.modify:

                                                System.out.println("Modify");
                                                Intent intent = new Intent(context, ModifyAccount.class);
                                                intent.putExtra("account_name", name_str);
                                                intent.putExtra("account_right", right_str);
                                                intent.putExtra("account_status", status_str);
//                                                context.startActivity(intent);

                                                ((Activity) mContext).startActivityForResult(intent,8);

                                                break;
                                            case R.id.delete:
                                                System.out.println("delete");
                                                DataBaseConnection connection =
                                                        new DataBaseConnection(Configure.URL_DELETE_ACCOUNT, name_str);
                                                connection.delete(adapter, AccountHolder.this);

                                                break;
                                            case R.id.print:
                                                System.out.println("print");
                                                break;
                                        }
                                    }
                                }).show();
                    }
                    else if(status_str.equals("d")){
                        sheet = new BottomSheet.Builder((Activity) mContext)
                                .sheet(R.menu.deleted_bottom_menu)
                                .title("Operating " + name_str)
                                .listener(new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case R.id.undelete:
                                                System.out.println("undelete");
                                                DataBaseConnection connection =
                                                        new DataBaseConnection(Configure.URL_UNDELETE_ACCOUNT, name_str);
                                                connection.undelete(adapter, AccountHolder.this);

                                                break;
                                            case R.id.print:
                                                System.out.println("print");
                                                break;
                                        }
                                    }
                                }).show();

                    }


                }
            });
        }
        public TextView getNameView() {
            return name;
        }

        public TextView getRightView() {
            return right;
        }
        public TextView getStatusView() {
            return status;
        }
        public ImageView getArrowView() {
            return arrow;
        }

    }

}
