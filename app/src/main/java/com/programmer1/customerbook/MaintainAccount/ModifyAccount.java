package com.programmer1.customerbook.MaintainAccount;

/**
 * Created by Programmer 1 on 2018-01-17.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.Login.LoginActivity;
import com.programmer1.customerbook.R;
import com.programmer1.customerbook.helper.SQLiteHandler;
import com.programmer1.customerbook.helper.SessionManager;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;


import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class ModifyAccount extends AppCompatActivity {

    private TextView name;
    private Button btnSave;
    private EditText inputRight;
    private EditText inputPassword;
    private EditText inputStatus;
    private SessionManager session;
    private SQLiteHandler db;
    private ProgressDialog pDialog;
    private HashMap<EditText, Boolean> editTextHashMap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_account);

        // Session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        name = findViewById(R.id.account_name);
        inputPassword = findViewById(R.id.reset_password);
        inputRight= findViewById(R.id.reset_right);
        inputStatus = findViewById(R.id.reset_status);
        btnSave = findViewById(R.id.saveAccount);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        Toolbar toolbar = findViewById(R.id.staff_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        name.setText(getIntent().getStringExtra("account_name"));
        inputRight.setText(getIntent().getStringExtra("account_right"));
        inputPassword.setText("******");
        inputStatus.setText(getIntent().getStringExtra("account_status"));

        editTextHashMap = new HashMap<>();
        editTextHashMap.put(inputPassword, false);
        editTextHashMap.put(inputRight, false);
        editTextHashMap.put(inputStatus, false);

        addTextChangeListener(inputPassword, editTextHashMap);
        addTextChangeListener(inputRight, editTextHashMap);
        addTextChangeListener(inputStatus, editTextHashMap);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HashMap<String, String> dataHashMap = new HashMap<>();
                String name_str = name.getText().toString();

                // Get a set of the entries
                Set set = editTextHashMap.entrySet();

                // Get an iterator
                Iterator i = set.iterator();

                // Display elements
                while(i.hasNext()) {
                    Map.Entry me = (Map.Entry)i.next();
                    EditText text = (EditText) me.getKey();

                    switch (text.getId()) {

                        case R.id.reset_password:
                            System.out.print( "Password: ");
                            Boolean check = (Boolean) me.getValue();
                            if(check){
                                dataHashMap.put("password", inputPassword.getText().toString());
                            }
                            break;

                        case R.id.reset_right:
                            System.out.print( "right: ");
                            check = (Boolean) me.getValue();
                            if(check){
                                dataHashMap.put("right", inputRight.getText().toString());
                            }
                            break;

                        case R.id.reset_status:
                            System.out.print( "status: ");
                            check = (Boolean) me.getValue();
                            if(check){
                                dataHashMap.put("status", inputStatus.getText().toString());
                            }
                            break;
                        default:
                            break;

                    }


                    System.out.println(me.getValue());
                }

                System.out.println();
                System.out.println();

                if(dataHashMap.isEmpty()){
                    Toast.makeText(getApplicationContext(),
                            "Nothing changed", Toast.LENGTH_LONG).show();
                }
                else{
                    update(name_str, dataHashMap);
                }
            }
        });
    }


    private void update(final String name, HashMap<String, String> map) {

        pDialog.setMessage("Modifying ...");
        showDialog();


        Set set = map.entrySet();
        Iterator i = set.iterator();

        String json_str ="{\"name\": \"" + name +"\", " + "\"data\": [";
        while(i.hasNext()) {
            Map.Entry me = (Map.Entry)i.next();
            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue());
            json_str += "{\"" + me.getKey().toString() + "\": \"" + me.getValue().toString() + "\"}," ;
        }

        json_str=json_str.substring(0, json_str.length()-1)+"]}";
        System.out.println(json_str);

        final MediaType mediaType = MediaType.parse("application/json");



        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        // GET request
        Request request = new Request.Builder()
                .url(Configure.URL_UPDATE_ACCOUNT)
                .post(RequestBody.create(mediaType, json_str))
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        hideDialog();
                        Toast.makeText(getApplicationContext(),
                                "Cannot connect to server", Toast.LENGTH_LONG).show();

                    }
                });

                System.out.println(e);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                hideDialog();
                final String res = response.body().string();
                String status = null;
                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(res);
                    status = Jobject.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String finalStatus = status;
                final JSONObject finalJobject = Jobject;


                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        switch (finalStatus) {
                            case "200":
                                Toast.makeText(getApplicationContext(),
                                        "Modify successfully", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent();
                                intent.putExtra("key", name);
                                setResult(RESULT_OK, intent);
                                finish();
                                break;

                            case "400":
                                Toast.makeText(getApplicationContext(),
                                        "Wrong data", Toast.LENGTH_LONG).show();
                                break;
                            case "404":
                                Toast.makeText(getApplicationContext(),
                                        "User account no found", Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(getApplicationContext(),
                                        "Server is busy, try again", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

        });

    }

    public void addTextChangeListener(final EditText input, final HashMap<EditText, Boolean> map){
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                map.put(input, true);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(ModifyAccount.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}