package com.programmer1.customerbook.MaintainAccount;

/**
 * Created by aaron on 1/23/2018.
 */

public class Account {

    private String name;
    private String right;
    private String status;


    public Account(String name, String right, String status) {
        this.name = name;
        this.right = right;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    public String getStatus() {
        return status;
    }

}
