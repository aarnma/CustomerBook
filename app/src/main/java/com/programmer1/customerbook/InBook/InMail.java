package com.programmer1.customerbook.InBook;

/**
 * Created by aaron on 2/27/2018.
 */

public class InMail {

    private String number;
    private String date;
    private String from;
    private String tel;
    private String method;                                                                                                                                                                                                                              ;
    private String subject;
    private String pages;
    private String folder_name;
    private String location;



    public InMail(String number, String date, String from, String tel, String method, String subject,
                    String pages, String folder_name, String location) {
        this.date = date;
        this.number = number;
        this.from = from;
        this.tel = tel;
        this.method = method;
        this.subject = subject;
        this.pages = pages;
        this.folder_name = folder_name;
        this.location = location;
    }

    public String getNumber() {
        return number;
    }

    public String getDate() {
        return date;
    }

    public String getFrom() {
        return from;
    }

    public String getTel() {
        return tel;
    }

    public String getMethod() {
        return method;
    }

    public String getSubject() {
        return subject;
    }

    public String getPages() {
        return pages;
    }

    public String getFolder_name() {
        return folder_name;
    }

    public String getLocation() {
        return location;
    }
}
