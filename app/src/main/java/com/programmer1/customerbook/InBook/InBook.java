package com.programmer1.customerbook.InBook;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import com.programmer1.customerbook.AaronSearch;
import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.Login.LoginActivity;
import com.programmer1.customerbook.R;
import com.programmer1.customerbook.helper.SQLiteHandler;
import com.programmer1.customerbook.helper.SessionManager;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.programmer1.customerbook.Configure.URL_IN_HINT;


/**
 * Created by aaron on 2/27/2018.
 */

public class InBook extends AppCompatActivity {

    private SQLiteHandler db;
    private SessionManager session;
    private AaronSearch searchView;
    private ProgressDialog pDialog;
    private ArrayList<InMail> search_result;
    private com.github.clans.fab.FloatingActionButton fab_in;
    private InMailFragment fragment;
    private String right;


    private final int RC_SEARCH = 1;
    private final int INTERVAL = 500;
    //private EditText mEtHandler;
    private Handler mHandler;
    private String key = "";

    private String[] suggestion_results;

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_book);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);


        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        HashMap<String, String> user = db.getUserDetails();

        right = user.get("right");

        if (!session.isLoggedIn()) {
            logoutUser();
        }

//        setDefaultSearch(searchView);
        fab_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        AddInMail.class);
                startActivityForResult(i,10);
            }
        });

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == RC_SEARCH) {
                    if(!key.equals("")){
                        System.out.println("searching"+ key);
                        getSearchSuggestions(URL_IN_HINT, key);
                    }

                }
            }
        };
        setSearch(searchView, Configure.URL_SEARCH_IN, "Search In Mail");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_bar, menu);
        searchView.setMenuItem(menu.findItem(R.id.action_search));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.action_print:
                Intent intent = new Intent(getApplicationContext(), PrintInMail.class);
                startActivity(intent);
                return true;

            default:
                return false;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10) {
            if(resultCode == RESULT_OK) {
                try {
                    stringToJson(data.getStringExtra("data"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                fragment = new InMailFragment(search_result, right, getApplicationContext());
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.in_fragment_container, fragment)
                        .commit();

            }
        }
    }


    private void setSearch(final AaronSearch searchView, final String url, final String hint) {

        if (searchView != null) {
            searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    getSearchResults(url, query);
                    searchView.closeSearch();
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    key = newText;
                    if (mHandler.hasMessages(RC_SEARCH)) {
                        mHandler.removeMessages(RC_SEARCH);
                    }
                    mHandler.sendEmptyMessageDelayed(RC_SEARCH, INTERVAL);

                    return true;
                }
            });
            searchView.setHint(hint);
        }
    }

    public void getSearchResults(String url, String query){

        pDialog.setMessage("Searching " + query);
        showDialog();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        RequestBody form = new FormBody.Builder()
                .add("query", query)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     hideDialog();
                                     Toast.makeText(getApplicationContext(),
                                             "Search server disconnected", Toast.LENGTH_LONG).show();
                                 }
                             });
                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             hideDialog();

                             final String res = response.body().string();
                             System.out.println(res);
                             String status = null;
                             JSONObject Jobject = null;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");
                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status != null ? status : "500";

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     if(finalStatus != null){
                                         switch (finalStatus) {
                                             case "200":

                                                 try {
                                                     stringToJson(res);
                                                     System.out.println(search_result.size());
                                                     System.out.println(search_result.get(0));
                                                     fragment = new InMailFragment(search_result, right, getApplicationContext());
                                                     getFragmentManager()
                                                             .beginTransaction()
                                                             .replace(R.id.in_fragment_container, fragment)
                                                             .commit();

                                                 } catch (JSONException e) {
                                                     e.printStackTrace();
                                                 }


                                                 break;

                                             case "404":
                                                 Toast.makeText(getApplicationContext(),
                                                         "InMail not found", Toast.LENGTH_LONG).show();

                                                 if(fragment != null){
                                                     getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentById(R.id.in_fragment_container)).commit();
                                                 }
                                                 fragment = null;

                                                 break;

                                             default:
                                                 Toast.makeText(getApplicationContext(),
                                                         "Server error, try again", Toast.LENGTH_LONG).show();
                                                 break;
                                         }
                                     }
                                 }
                             });
                         }
                     }
        );


    }

    public void stringToJson(String str) throws JSONException {

        search_result = new ArrayList<>();
        JSONObject Jobject = new JSONObject(str);
        JSONArray Jarray = Jobject.getJSONArray("output");

        for (int i = 0; i < Jarray.length(); i++) {
            JSONObject object  = Jarray.getJSONObject(i);

            InMail in = new InMail(object.getString("ref_num"),
                    object.getString("date"),  object.getString("from"),
                    object.getString("tel"), object.getString("method"),
                    object.getString("subject"), object.getString("pages"),
                    object.getString("folder_name"), object.getString("location"));

            search_result.add(in);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }






    public void addToSuggestionArray(String str) throws JSONException {

        ArrayList<String> suggestions= new ArrayList<>();
        JSONObject Jobject = new JSONObject(str);
        JSONArray Jarray = Jobject.getJSONArray("output");

        for (int i = 0; i < Jarray.length(); i++) {
            JSONObject object  = Jarray.getJSONObject(i);
            String hint = object.getString("hint");
            if (!suggestions.contains(hint)){
                suggestions.add(hint);
            }

        }
        suggestion_results = suggestions.toArray(new String[suggestions.size()]);
    }



    public void getSearchSuggestions(String url, String query){
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        RequestBody form = new FormBody.Builder()
                .add("query", query)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     hideDialog();
                                     Toast.makeText(getApplicationContext(),
                                             "Suggestion Server disconnected", Toast.LENGTH_LONG).show();
                                 }
                             });
                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             hideDialog();

                             final String res = response.body().string();
                             System.out.println(res);
                             String status = null;
                             JSONObject Jobject = null;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");
                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status != null ? status : "500";

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     if(finalStatus != null){
                                         switch (finalStatus) {
                                             case "200":

                                                 try {
                                                     addToSuggestionArray(res);
                                                     searchView.setSuggestions(suggestion_results);
                                                     System.out.print(suggestion_results[0]);
                                                     System.out.println("suggestions set");


                                                 } catch (JSONException e) {
                                                     e.printStackTrace();
                                                 }


                                                 break;

                                             case "404":
                                                 Toast.makeText(getApplicationContext(),
                                                         "Suggestions not found", Toast.LENGTH_LONG).show();


                                                 break;

                                             case "500":
                                                 Toast.makeText(getApplicationContext(),
                                                         "Suggestions Server error, try again", Toast.LENGTH_LONG).show();
                                                 break;
                                         }
                                     }
                                 }
                             });
                         }
                     }
        );


    }
}

