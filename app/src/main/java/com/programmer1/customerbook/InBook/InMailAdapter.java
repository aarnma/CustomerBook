package com.programmer1.customerbook.InBook;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cocosw.bottomsheet.BottomSheet;
import com.programmer1.customerbook.R;

import java.util.ArrayList;

/**
 * Created by aaron on 2/27/2018.
 */

public class InMailAdapter extends RecyclerView.Adapter<InMailAdapter.InMailHolder> {
    private Activity context;
    private ArrayList<InMail> inmails;
    private String right;


    public InMailAdapter(Activity context, ArrayList<InMail> inmails, String right) {
        this.context = context;
        this.inmails = inmails;
        this.right = right;

    }

    @Override
    public InMailAdapter.InMailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InMailAdapter.InMailHolder(this, context,
                LayoutInflater.from(parent.getContext()).inflate(R.layout.in_mail_layout, parent, false), right);
    }


    @Override
    public void onBindViewHolder(final InMailAdapter.InMailHolder holder, int position) {
        InMail mail = inmails.get(position);

        holder.getNumber().setText(mail.getNumber());
        holder.getDate().setText(mail.getDate());
        holder.getFrom().setText(mail.getFrom());
        holder.getTel().setText(mail.getTel());
        holder.getMethod().setText(mail.getMethod());
        holder.getSubject().setText(mail.getSubject());
        holder.getPages().setText(mail.getPages());
        holder.getFolder_name().setText(mail.getFolder_name());
        holder.getLocation().setText(mail.getLocation());


    }

    @Override
    public int getItemCount() {
        return inmails.size();
    }


    public class InMailHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


        protected TextView number;
        protected TextView date;
        protected TextView from;
        protected TextView tel;
        protected TextView method;
        protected TextView subject;
        protected TextView pages;
        protected TextView folder_name;
        protected TextView location;
        protected Activity mContext;

        public TextView getNumber() {
            return number;
        }

        public TextView getDate() {
            return date;
        }

        public TextView getFrom() {
            return from;
        }

        public TextView getTel() {
            return tel;
        }

        public TextView getMethod() {
            return method;
        }

        public TextView getSubject() {
            return subject;
        }

        public TextView getPages() {
            return pages;
        }

        public TextView getFolder_name() {
            return folder_name;
        }

        public TextView getLocation() {
            return location;
        }

        public InMailHolder(InMailAdapter inMailAdapter, Activity context, View itemView, String right) {
            super(itemView);
            number = itemView.findViewById(R.id.reference_in_num_content);
            date = itemView.findViewById(R.id.in_date_content);
            from = itemView.findViewById(R.id.from_content);
            tel = itemView.findViewById(R.id.tel_content);
            method = itemView.findViewById(R.id.method_content);
            subject = itemView.findViewById(R.id.subject_content);
            pages = itemView.findViewById(R.id.pages_content);
            folder_name = itemView.findViewById(R.id.folder_name_content);
            location = itemView.findViewById(R.id.location_content);
            mContext = context;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            final String ref_num = number.getText().toString();
            final String pages_str = pages.getText().toString();

            BottomSheet.Builder btsheet = new BottomSheet.Builder(mContext);
            btsheet.sheet(R.menu.bottom_menu_modify);
            btsheet.title("Operating " + ref_num)
                    .listener(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case R.id.modify:

                                    Intent intent = new Intent(context, ModifyInMail.class);
                                    intent.putExtra("ref_num", ref_num);
                                    intent.putExtra("pages", pages_str);
                                    ((Activity) mContext).startActivityForResult(intent,9);

                                    break;
                            }
                        }
                    }).show();

        }
    }
}