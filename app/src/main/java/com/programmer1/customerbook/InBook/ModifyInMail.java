package com.programmer1.customerbook.InBook;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.programmer1.customerbook.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.programmer1.customerbook.Configure.HttpPost;
import static com.programmer1.customerbook.Configure.URL_MODIFY_IN;

/**
 * Created by aaron on 4/12/2018.
 */

public class ModifyInMail extends AppCompatActivity{

    private Button btnSave;
    private EditText pages;
    private JSONObject data = new JSONObject();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_inmail);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        // Progress dialog

        pages = findViewById(R.id.modify_inmail_pages_content);
        btnSave = findViewById(R.id.btnModify);

        Toolbar toolbar = findViewById(R.id.modify_inmail_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        pages.setText(getIntent().getStringExtra("pages"));
        final String key = getIntent().getStringExtra("ref_num");

        addTextChangeListener(pages);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.length() == 0) {
                    Toast.makeText(getApplicationContext(),
                            "Nothing Changed", Toast.LENGTH_LONG).show();

                } else {
                    try {
                        data.put("ref_num", key);
                        HttpPost(ModifyInMail.this, URL_MODIFY_IN, data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }}
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void addTextChangeListener(final EditText input){
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                try {
                    data.put(input.getTag().toString(), charSequence.toString().trim());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
    }
}
