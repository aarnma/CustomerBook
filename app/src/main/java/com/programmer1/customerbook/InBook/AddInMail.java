package com.programmer1.customerbook.InBook;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.R;
import com.programmer1.customerbook.helper.SQLiteHandler;
import com.programmer1.customerbook.helper.SessionManager;
import com.isapanah.awesomespinner.AwesomeSpinner;
import com.reginald.editspinner.EditSpinner;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.programmer1.customerbook.ReceiptBook.AddReceipt.stringToJson;

/**
 * Created by aaron on 2/27/2018.
 */

public class AddInMail extends AppCompatActivity {

    private Button add_in;
    private EditText inputFrom;
    private EditSpinner inputTel;
    private EditText inputFolderName;
    private EditText inputSubject;
    private EditText inputPages;
    private AwesomeSpinner method;
    private AwesomeSpinner location;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;
    private ArrayList<String> phone_num_list;

    private AVLoadingIndicatorView loading_phone;
    private TextView phone_num_msg;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_in);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        inputFrom = findViewById(R.id.in_from);
        inputTel = findViewById(R.id.in_tel);
        inputFolderName = findViewById(R.id.in_folder_name);
        inputSubject = findViewById(R.id.in_subject);
        inputPages = findViewById(R.id.in_pages);
        method = findViewById(R.id.in_method);
        add_in = findViewById(R.id.btnAddIn);
        location = findViewById(R.id.in_location);
        loading_phone  = findViewById(R.id.in_avi);
        phone_num_msg = findViewById(R.id.in_phone_msg);

        Toolbar toolbar = findViewById(R.id.add_in_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");


        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        ArrayAdapter<CharSequence> staffAdapter = ArrayAdapter.createFromResource(this, R.array.method, android.R.layout.simple_spinner_item);
        method.setAdapter(staffAdapter, 0);

        ArrayAdapter<CharSequence> branchAdapter = ArrayAdapter.createFromResource(this, R.array.location, android.R.layout.simple_spinner_item);
        location.setAdapter(branchAdapter, 0);

        inputFrom.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(!hasFocus){
                    String name = inputFrom.getText().toString().trim();
                    if(!name.isEmpty()&& name.contains(" ")){
                        loading_phone.setVisibility(View.VISIBLE);
                        loading_phone.show();
                        phone_num_msg.setVisibility(View.VISIBLE);
                        phone_num_msg.setText("Loading Phone Number");
                        String parts[] = name.split(" ", 2);
                        getNums(Configure.URL_SEARCH_PHONE_NUM, parts[0], parts[1]);

                    }
                }
            }
        });
        method.setOnSpinnerItemClickListener(new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int position, String itemAtPosition) {
                hideKeyboard(AddInMail.this);

            }
        });

        location.setOnSpinnerItemClickListener(new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int position, String itemAtPosition) {
                hideKeyboard(AddInMail.this);

            }
        });


        // triggered when dropdown popup window shown
        inputTel.setOnShowListener(new EditSpinner.OnShowListener() {
            @Override
            public void onShow() {
                // maybe you want to hide the soft input panel when the popup window is showing.
                hideKeyboard(AddInMail.this);
            }
        });


        add_in.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String form = inputFrom.getText().toString();
                String tel = inputTel.getText().toString();
                String folder_name = inputFolderName.getText().toString();
                String subject = inputSubject.getText().toString();
                String pages = inputPages.getText().toString();
                String method_sel = method.getSelectedItem();
                String location_sel = location.getSelectedItem();

                if (!form.isEmpty() &&
                        !folder_name.isEmpty() && method.isSelected() && !subject.isEmpty()
                        && location.isSelected()) {
                    add_in_to_database(form, tel,
                            folder_name, subject, pages, method_sel, location_sel);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please fill up all fields", Toast.LENGTH_LONG)
                            .show();
                }


            }
        });


    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void add_in_to_database(final String form_str,
                                    final String tel, final String folder_name
            , final String subject, final String pages, final String method_sel, final String location_sel
    ) {

        pDialog.setMessage("Adding ...");
        showDialog();

        RequestBody form = new FormBody.Builder()
                .add("form", form_str)
                .add("tel", tel)
                .add("folder_name", folder_name)
                .add("subject", subject)
                .add("pages", pages)
                .add("method_sel", method_sel)
                .add("location_sel", location_sel)
                .build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        // GET request
        Request request = new Request.Builder()
                .url(Configure.URL_ADD_IN)
                .post(form)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideDialog();
                        Toast.makeText(getApplicationContext(),
                                "Server is disconnected", Toast.LENGTH_LONG).show();

                    }
                });
                System.out.println(e);

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                hideDialog();
                final String res = response.body().string();

                String status = null;
                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(res);
                    status = Jobject.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String finalStatus = status != null ? status : "500";


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (finalStatus) {
                            case "200":
                                Toast.makeText(getApplicationContext(),
                                        "New InMail added", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent();
                                intent.putExtra("data", res);
                                setResult(RESULT_OK, intent);
                                finish();
                                break;

                            case "500":
                                Toast.makeText(getApplicationContext(),
                                        "Server is busy", Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(getApplicationContext(),
                                        "Server is disconnected", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    public void getNums(String url, String lastname, String firstname){


        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        RequestBody form = new FormBody.Builder()
                .add("firstname", firstname)
                .add("lastname", lastname)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {

                                     loading_phone.hide();
                                     inputTel.setText("");
                                     phone_num_msg.setTextColor(Color.parseColor("#DC143C"));
                                     phone_num_msg.setText("Server down");
                                 }
                             });
                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             String status = null;
                             JSONObject Jobject = null;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");

                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status != null ? status : "500";

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     assert finalStatus != null;
                                     switch (finalStatus) {
                                         case "200":
                                             try {
                                                 phone_num_list = new ArrayList<>();
                                                 stringToJson(res, phone_num_list, "phone");
                                                 ListAdapter  phonenumAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_dropdown_item,
                                                         phone_num_list);
                                                 inputTel.setAdapter(phonenumAdapter);
//                                                 inputTel.setDropDownBackgroundResource(R.layout.spinner_dropdown_item);
                                                 loading_phone.hide();
                                                 phone_num_msg.setTextColor(Color.parseColor("#26ae90"));
                                                 phone_num_msg.setText("Successful, click to select!");

                                             } catch (JSONException e) {
                                                 e.printStackTrace();
                                             }

                                             break;

                                         case "404":
                                             if(phone_num_list != null){
                                                 inputTel.setAdapter(null);
                                             }
                                             inputTel.setText("");
                                             loading_phone.hide();
                                             phone_num_msg.setTextColor(Color.parseColor("#DC143C"));
                                             phone_num_msg.setText("No found, name is wrong!");
                                             break;
                                         default:
                                             loading_phone.hide();
                                             inputTel.setText("");
                                             phone_num_msg.setTextColor(Color.parseColor("#DC143C"));
                                             phone_num_msg.setText("Server error, try again");
                                             break;
                                     }
                                 }
                             });
                         }
                     }
        );


    }
}