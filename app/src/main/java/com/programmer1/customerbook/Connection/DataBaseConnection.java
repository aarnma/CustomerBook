package com.programmer1.customerbook.Connection;

import android.app.Activity;

import com.programmer1.customerbook.ContractBook.ContractAdapter;
import com.programmer1.customerbook.MaintainAccount.AccountAdapter;
import com.programmer1.customerbook.ReceiptBook.ReceiptAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by aaron on 1/25/2018.
 */

public class DataBaseConnection extends Activity {


    private String url;
    private String name;
    private int key;


    public DataBaseConnection(String url, String name) {
        this.url = url;
        this.name = name;
    }

    public DataBaseConnection(String url, int key) {
        this.url = url;
        this.key = key;
    }


    public void delete(final AccountAdapter adapter, final AccountAdapter.AccountHolder holder){

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        RequestBody form = new FormBody.Builder()
                .add("name", name)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {

                                     System.out.println("Server Error");
                                 }
                             });

                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             String status = null;
                             JSONObject Jobject;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");
                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status;

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     switch (finalStatus) {
                                         case "200":
//                                             Toast.makeText(getApplicationContext(),
//                                                     "Deleted", Toast.LENGTH_LONG).show();
                                              System.out.println("200");
                                             adapter.removeAt(holder.getPosition());

                                             break;
                                         case "404":
                                             System.out.println("400");

                                             break;
                                         default:
                                             System.out.println("500");
                                             break;
                                     }
                                 }
                             });
                         }
                     }
        );
    }


    public void undelete(final AccountAdapter adapter, final AccountAdapter.AccountHolder holder){

        OkHttpClient client = new OkHttpClient();

        RequestBody form = new FormBody.Builder()
                .add("name", name)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {

                                     System.out.println("Server Error");
                                 }
                             });

                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             String status = null;
                             JSONObject Jobject;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");
                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status;

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     switch (finalStatus) {
                                         case "200":
//                                             Toast.makeText(getApplicationContext(),
//                                                     "Deleted", Toast.LENGTH_LONG).show();
                                             System.out.println("200");
                                             adapter.removeAt(holder.getPosition());

                                             break;
                                         case "404":
                                             System.out.println("400");

                                             break;
                                         default:
                                             System.out.println("500");
                                             break;
                                     }
                                 }
                             });
                         }
                     }
        );
    }


    public void delete_contract(final ContractAdapter adapter, final ContractAdapter.ContractHolder holder){

        OkHttpClient client = new OkHttpClient();

        RequestBody form = new FormBody.Builder()
                .add("contract_num", String.valueOf(key))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {

                                     System.out.println("Server Error");
                                 }
                             });

                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             String status = null;
                             JSONObject Jobject;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");
                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status;

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     switch (finalStatus) {
                                         case "200":
//                                             Toast.makeText(getApplicationContext(),
//                                                     "Deleted", Toast.LENGTH_LONG).show();
                                             System.out.println("200");
                                             adapter.removeAt(holder.getPosition());

                                             break;
                                         case "404":
                                             System.out.println("400");

                                             break;
                                         default:
                                             System.out.println("500");
                                             break;
                                     }
                                 }
                             });
                         }
                     }
        );
    }


    public void undelete_contract(final ContractAdapter adapter, final ContractAdapter.ContractHolder holder){

        OkHttpClient client = new OkHttpClient();

        RequestBody form = new FormBody.Builder()
                .add("contract_num", String.valueOf(key))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {

                                     System.out.println("Server Error");
                                 }
                             });

                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             String status = null;
                             JSONObject Jobject;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");
                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status;

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     switch (finalStatus) {
                                         case "200":

                                             System.out.println("200");
                                             adapter.removeAt(holder.getPosition());

                                             break;
                                         case "404":
                                             System.out.println("400");

                                             break;
                                         default:
                                             System.out.println("500");
                                             break;
                                     }
                                 }
                             });
                         }
                     }
        );
    }

    public void delete_receipt(final ReceiptAdapter adapter, final ReceiptAdapter.ReceiptHolder holder){

        OkHttpClient client = new OkHttpClient();

        RequestBody form = new FormBody.Builder()
                .add("receipt_num", String.valueOf(key))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {

                                     System.out.println("Server Error");
                                 }
                             });

                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             String status = null;
                             JSONObject Jobject;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");
                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status;

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     switch (finalStatus) {
                                         case "200":
//                                             Toast.makeText(getApplicationContext(),
//                                                     "Deleted", Toast.LENGTH_LONG).show();
                                             System.out.println("200");
                                             adapter.removeAt(holder.getPosition());

                                             break;
                                         case "404":
                                             System.out.println("400");

                                             break;
                                         default:
                                             System.out.println("500");
                                             break;
                                     }
                                 }
                             });
                         }
                     }
        );
    }


    public void undelete_receipt(final ReceiptAdapter adapter, final  ReceiptAdapter.ReceiptHolder holder){

        OkHttpClient client = new OkHttpClient();

        RequestBody form = new FormBody.Builder()
                .add("receipt_num", String.valueOf(key))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback()
                     {

                         @Override
                         public void onFailure(Call call, IOException e) {

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {

                                     System.out.println("Server Error");
                                 }
                             });

                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             String status = null;
                             JSONObject Jobject;
                             try {
                                 Jobject = new JSONObject(res);
                                 status = Jobject.getString("status");
                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }

                             final String finalStatus = status;

                             runOnUiThread(new Runnable()
                             {
                                 @Override
                                 public void run()
                                 {
                                     switch (finalStatus) {
                                         case "200":
//                                             Toast.makeText(getApplicationContext(),
//                                                     "Deleted", Toast.LENGTH_LONG).show();
                                             System.out.println("200");
                                             adapter.removeAt(holder.getPosition());

                                             break;
                                         case "404":
                                             System.out.println("400");

                                             break;
                                         default:
                                             System.out.println("500");
                                             break;
                                     }
                                 }
                             });
                         }
                     }
        );
    }


}
