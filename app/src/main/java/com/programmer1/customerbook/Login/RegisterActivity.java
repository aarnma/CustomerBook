package com.programmer1.customerbook.Login;

/**
 * Created by Programmer 1 on 2018-01-17.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.programmer1.customerbook.Configure;
import com.programmer1.customerbook.R;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class RegisterActivity extends AppCompatActivity {
    private Button btnRegister;
    private EditText inputFullName;
    private EditText inputPassword;
    private EditText inputPriority;
    private ProgressDialog pDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        inputFullName = findViewById(R.id.name);
        inputPassword = findViewById(R.id.re_password);
        inputPriority = findViewById(R.id.priority);
        btnRegister = findViewById(R.id.btnRegister);

        Toolbar toolbar = findViewById(R.id.register_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);


        // Register Button Click event
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String name = inputFullName.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();
                String right = inputPriority.getText().toString().trim();

                if (!name.isEmpty() && !password.isEmpty() && !right.isEmpty()) {
                    registerUser(name, password, right);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter both username and password", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });


    }

    private void registerUser(final String name,
                              final String password, final String right) {

        pDialog.setMessage("Registering ...");
        showDialog();

        RequestBody form = new FormBody.Builder()
                .add("name", name)
                .add("password", password)
                .add("right", right)
                .build();

        OkHttpClient client = new OkHttpClient();
        // GET request
        Request request = new Request.Builder()
                .url(Configure.URL_REGISTER)
                .post(form)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        hideDialog();
                        Toast.makeText(RegisterActivity.this,
                                "Register server disconnected", Toast.LENGTH_LONG).show();

                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                hideDialog();

                final String res = response.body().string();

                String status = null;
                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(res);
                    status = Jobject.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String finalStatus = status != null ? status : "500";

                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        switch (finalStatus) {
                            case "200":

                                Toast.makeText(getApplicationContext(),
                                        "User successfully registered. Try login now!", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent();
                                intent.putExtra("data", res);
                                setResult(RESULT_OK, intent);
                                finish();
                                break;
                            case "400":
                                Toast.makeText(getApplicationContext(),
                                        "Account existed", Toast.LENGTH_LONG).show();
                                break;
                            default:
                                Toast.makeText(getApplicationContext(),
                                        "Server is busy", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

        });

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}