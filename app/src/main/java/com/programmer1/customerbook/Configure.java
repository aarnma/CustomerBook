package com.programmer1.customerbook;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.programmer1.customerbook.Login.LoginActivity;
import com.programmer1.customerbook.helper.SQLiteHandler;
import com.programmer1.customerbook.helper.SendMail;
import com.programmer1.customerbook.helper.SessionManager;
import com.programmer1.customerbook.helper.SharedPreference;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Programmer 1 on 2018-01-18.
 */

public class Configure {

    public static final String EMAIL ="yijianoreply@gmail.com"; //your-gmail-username
    public static final String PASSWORD ="yijianoreply6042337072"; //your-gmail-password

//    // Account URL

    private static String URL = "http://35.197.7.124:5989/api/";

    public static String URL_LOGIN = URL + "login";

    public static String URL_REGISTER = URL +"register";

    public static String URL_SEARCH_DEFAULT = URL +"search_staff";

    public static String URL_SEARCH_DELETED = URL +"search_deleted_staff";

    public static String URL_DELETE_ACCOUNT = URL +"delete_staff";

    public static String URL_UNDELETE_ACCOUNT  = URL +"release_staff";

    public static String URL_UPDATE_ACCOUNT  = URL +"update_staff";


    //Contract URL
    public static String URL_ADD_CONTRACT  = URL +"add_contract";
    public static String URL_SEARCH_DEFAULT_CONTRACT  = URL +"search_contract";
    public static String URL_SEARCH_DELETED_CONTRACT  = URL +"search_deleted_contract";
    public static String URL_DELETE_CONTRACT  = URL +"delete_contract";
    public static String URL_UNDELETE_CONTRACT  = URL +"release_contract";
    public static String URL_UPDATE_CONTRACT  = URL +"update_contract";
    public static String URL_CONTRACT_HINT  = URL +"contract_hint";
    public static String URL_CALCULATE_CONTRACT  = URL +"calculate_contract";
    public static String URL_PRINT_CONTRACT  = URL +"print_contract";



    //Receipt URL
    public static String URL_ADD_RECEIPT  = URL +"add_receipt";
    public static String URL_SEARCH_DEFAULT_RECEIPT  = URL +"search_receipt";
    public static String URL_SEARCH_DELETED_RECEIPT  = URL +"search_deleted_receipt";
    public static String URL_DELETE_RECEIPT  = URL +"delete_receipt";
    public static String URL_UNDELETE_RECEIPT  = URL +"release_receipt";
    public static String URL_UPDATE_RECEIPT  = URL +"update_receipt";
    public static String URL_SEARCH_CONTRACT_NUM  = URL + "search_contract_num";
    public static String URL_GET_BALANCE  = URL +"get_balance";
    public static String URL_SEARCH_PHONE_NUM  = URL +"search_phone_num";
    public static String URL_RECEIPT_HINT  = URL +"receipt_hint";
    public static String URL_CALCULATE_RECEIPT  = URL +"calculate_receipt";
    public static String URL_PRINT_RECEIPT  = URL +"print_receipt";


    // In URL
    public static String URL_ADD_IN  = URL +"add_in";
    public static String URL_SEARCH_IN  = URL +"search_in";
    public static String URL_IN_HINT  = URL +"in_hint";
    public static String URL_PRINT_IN  = URL +"print_in";
    public static String URL_MODIFY_IN  = URL +"modify_in";
    public static String URL_SEARCH_REF_IN  = URL +"search_ref_in";



    // Out URL
    public static String URL_ADD_OUT  = URL +"add_out";
    public static String URL_SEARCH_OUT  = URL +"search_out";
    public static String URL_OUT_HINT  = URL +"out_hint";
    public static String URL_PRINT_OUT  = URL +"print_out";
    public static String URL_MODIFY_OUT  = URL +"modify_out";
    public static String URL_SEARCH_REF_OUT  = URL +"search_ref_out";


    public static final String newPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ReceiptMail/";
    private static final String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ReceiptMail";

    private static boolean externalStorageReadable, externalStorageWritable;
    private static EditText emailInput;

    public static boolean isExternalStorageReadable() {
        checkStorage();
        return externalStorageReadable;
    }

    public static boolean isExternalStorageWritable() {
        checkStorage();
        return externalStorageWritable;
    }

    public static boolean isExternalStorageReadableAndWritable() {
        checkStorage();
        return externalStorageReadable && externalStorageWritable;
    }

    private static void checkStorage() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            externalStorageReadable = externalStorageWritable = true;
        } else if (state.equals(Environment.MEDIA_MOUNTED) || state.equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
            externalStorageReadable = true;
            externalStorageWritable = false;
        } else {
            externalStorageReadable = externalStorageWritable = false;
        }
    }

    public static void logoutUser(SQLiteHandler db, SessionManager session, Activity activity) {
        session.setLogin(false);
        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    public static void HttpPost(final Activity activity, String url, JSONObject data) throws IOException {

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        RequestBody form = RequestBody.create(JSON, data.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
                         @Override
                         public void onFailure(Call call, IOException e) {

                             activity.runOnUiThread(new Runnable() {
                                 @Override
                                 public void run() {
                                     showMessage("Backend server is down, find IT", SweetAlertDialog.ERROR_TYPE, activity);
                                 }
                             });
                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             activity.runOnUiThread(new Runnable() {
                                 @Override
                                 public void run() {
                                     try {
                                         JSONObject Jobject = new JSONObject(res);
                                         String status = Jobject.getString("status");
                                         status = status != null ? status : "500";
                                         switch (status) {
                                             case "200":
                                                 Intent intent = new Intent();
                                                 intent.putExtra("key", Jobject.getString("key"));
                                                 activity.setResult(RESULT_OK, intent);
                                                 activity.finish();
                                                 break;
                                             case "500":
                                                 showMessage("Status is null", SweetAlertDialog.ERROR_TYPE, activity);
                                                 break;

                                         }
                                     } catch (JSONException e) {
                                         e.printStackTrace();
                                     }
                                 }
                             });
                         }
                     }
        );
    }

    private static void showMessage(String info, int type, Context context) {
        SweetAlertDialog pDialog = new SweetAlertDialog(context, type);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(info);
        pDialog.setCancelable(true);
        pDialog.show();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }



    public static void writeDoc(Activity activity, InputStream in, File newFile, Map<String, String> map, String filename) {
        try {

            File file = new File(filePath);
            if (!file.exists()) {
                file.mkdirs();
            }

            HWPFDocument hdt = new HWPFDocument(in);
            // Fields fields = hdt.getFields();
            // 读取word文本内容
            Range range = hdt.getRange();
            // System.out.println(range.text());

            // 替换文本内容
            for (Map.Entry<String, String> entry : map.entrySet()) {
                range.replaceText(entry.getKey(), entry.getValue());
            }
            ByteArrayOutputStream ostream = new ByteArrayOutputStream();
            FileOutputStream out = new FileOutputStream(newFile, true);
            hdt.write(ostream);
            // 输出字节流
            out.write(ostream.toByteArray());
            out.close();
            ostream.close();

//            Toast.makeText(activity,
//                    "receipt generated", Toast.LENGTH_LONG).show();
            showInputDialog(activity, filename);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void SaveReceiptToLocal(Activity activity, JSONObject jsonObject) {

        try {
            //从assets读取我们的Word模板
            InputStream is = activity.getAssets().open("receipt.doc");
            //创建生成的文件

            try {

                String filename = jsonObject.getString("receipt_num") + "_receipt.doc";
                String file_path = newPath + filename;
                File newFile = new File(file_path);

                Map<String, String> map = new HashMap<String, String>();
                map.put("$Date$",  jsonObject.getString("date"));
                map.put("$From$",  jsonObject.getString("lastname") + " " +  jsonObject.getString("firstname"));
                map.put("$ContractNum$",  jsonObject.getString("contract_num"));
                map.put("$Class$",  jsonObject.getString("class"));
                map.put("$Remark$",  jsonObject.getString("memo"));
                map.put("$GST$",  jsonObject.getString("gst"));
                map.put("$Total$",  jsonObject.getString("total_amount"));
                map.put("$ReceiptNum$",  jsonObject.getString("receipt_num"));

                String method = jsonObject.getString("payment_method");

                switch (method){
                    case "Cash":
                        map.put("$Amount1$",  "");
                        map.put("$Amount2$",  jsonObject.getString("amount"));
                        map.put("$Amount3$",  "");
                        break;
                    case "Credit Card":
                        map.put("$Amount1$",  "");
                        map.put("$Amount2$",  "");
                        map.put("$Amount3$",  jsonObject.getString("amount"));
                        break;
                    case "Debit Card":
                        map.put("$Amount1$",  "");
                        map.put("$Amount2$",  "");
                        map.put("$Amount3$",  jsonObject.getString("amount"));
                        break;
                    case "Cheque":
                        map.put("$Amount1$",  jsonObject.getString("amount"));
                        map.put("$Amount2$",  "");
                        map.put("$Amount3$",  "");
                        break;

                    default:
                        map.put("$Amount1$",  "");
                        map.put("$Amount2$",  "");
                        map.put("$Amount3$",  jsonObject.getString("amount"));
                }
                writeDoc(activity, is, newFile, map, filename);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showInputDialog(final Activity activity, final String subject) {
        MaterialDialog dialog =
                new MaterialDialog.Builder(activity)
                        .title("请输入您的邮箱地址")
                        .customView(R.layout.custom_dialog_view, true)
                        .positiveText("确认")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                String email_address = emailInput.getText().toString().trim();
                                SharedPreference.getInstance(activity).setValue("email", email_address);

                                SendMail sm = new SendMail(activity, email_address, subject, "Find your receipt at attachment");
                                sm.execute();
                            }
                        })
                        .build();

        final View positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        final TextView warning = dialog.getCustomView().findViewById(R.id.email_address_warning);

        emailInput = dialog.getCustomView().findViewById(R.id.email_address);
        emailInput.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if(!Patterns.EMAIL_ADDRESS.matcher(s).matches()){
                            positiveAction.setEnabled(false);
                            warning.setVisibility(View.VISIBLE);
                        }else {
                            positiveAction.setEnabled(true);
                            warning.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void afterTextChanged(Editable s) {}
                });
        emailInput.setText(SharedPreference.getInstance(activity)
                .getStringValue("email", "2337072@gmail.com"));
        dialog.show();
    }
}
